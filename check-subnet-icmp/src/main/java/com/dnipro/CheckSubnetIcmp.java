package com.dnipro;


import com.dnipro.model.monitoring.MonitoringCheckResultDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import java.io.Serializable;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Slf4j
public class CheckSubnetIcmp extends BaseCommand<MonitoringCheckResultDTO> implements Serializable {

    @Override
    protected MonitoringCheckResultDTO exec() {
        MonitoringCheckResultDTO monitoringCheckResultDTO = MonitoringCheckResultDTO.builder()
                .agentUUID(getAgentUUID())
                .commandUUID(getCommandUUID())
                .isAddNew(true)
                .build();
        List<Map<String,String>> result = new ArrayList<>();
        final String subnet = getProperties().get("host");
        if (StringUtils.isBlank(subnet)) {
            log.info("StringUtils.isBlank(subnet) - {}",subnet);
            throw new IllegalArgumentException("Host is undefined");
        }

        final String[] data = subnet.split("/");
        String[] ip = data[0].split("\\.");
        int mask = Integer.parseInt(data[1]);
        try {
            for (int i = 25 - mask; i > 0; i--) {
                String checkSubnet = ip[0] + "." + ip[1] + "." + ip[2] + ".";
                for (int k = 1; k < 255; k++) {
                    final String host = checkSubnet + String.valueOf(k);
                    InetAddress inetAddress  = InetAddress.getByName(host);
                    if (inetAddress.isReachable(50)) {
                        Map<String,String> checkResult = new HashMap<>();
                        checkResult.put("host", host);
                        checkResult.put("isActive", "true");
                        result.add(checkResult);
                    }
                    monitoringCheckResultDTO.setResult(result);
                }
                ip[2] = String.valueOf(Integer.valueOf(ip[2]) + 1);
            }
        } catch (Exception e) {
            log.error(StringUtils.EMPTY, e);
        }
        return monitoringCheckResultDTO;
    }
    @Override
    protected void stop() {

    }
}
