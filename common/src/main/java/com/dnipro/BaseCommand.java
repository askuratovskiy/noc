package com.dnipro;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

@Slf4j
@EqualsAndHashCode
public abstract class BaseCommand<R> implements AutoCloseable, Runnable, Cloneable {

    @Getter
    @Setter
    private Map<String, String> properties = Collections.synchronizedMap(new HashMap<>());

    @Getter
    @Setter
    private String jobId;

    @Getter
    @Setter
    private String commandUUID;

    @Getter
    @Setter
    private String agentUUID;

    @Setter
    @Getter
    private Date nextCheck = new Date(0);

    @Getter
    @Setter
    private Boolean isHostActive = null;

    @Getter
    @Setter
    private volatile boolean isActive = false;

    @Getter
    @Setter
    private volatile int checkInterval = 0;

    @Getter
    @Setter
    protected Consumer<R> resultConsumer;

    protected abstract R exec();

    protected abstract void stop();

    public void updateNextCheck() {
        Date currentTime = new Date();
        nextCheck = new Date(currentTime.getTime() + TimeUnit.MINUTES.toMillis(checkInterval));
    }

    @Override
    public final void run() {
        if (Objects.isNull(resultConsumer)) {
            throw new IllegalArgumentException("Result consumer is undefined");
        }
        try {
            resultConsumer.accept(exec());
        } catch (Exception e) {
            log.info("class BaseCommand<R>\ncalling resultConsumer.accept(exec())");
        }
//
//                if (checkInterval > 0) {
//                    try {
//                        Thread.sleep(TimeUnit.MINUTES.toMillis(checkInterval));
//                    } catch (InterruptedException e) {
//                        log.error(StringUtils.EMPTY, e);
//                    }
//                } else {
//                    close();
//                }
}

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public final void close() {
        setActive(false);
        stop();
    }
}
