package com.dnipro;



import com.dnipro.model.monitoring.MonitoringCheckResultDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.net.InetAddress;
import java.util.*;


@Slf4j
public class CheckDeviceIcmp extends BaseCommand<MonitoringCheckResultDTO> implements Serializable{

        private static final String DATA_SEPARATOR = "\n----------------------------------------\n";

    @Override
    protected MonitoringCheckResultDTO exec() {

        final String host = getProperties().get("host");
        Boolean resultIsHostActive = null;
        MonitoringCheckResultDTO monitoringCheckResultDTO = null;

        try {
            if (StringUtils.isBlank(host)) {
                throw new IllegalArgumentException("Host is undefined");
            }
            final InetAddress inet = InetAddress.getByName(host);
            if (inet.isReachable(500)) {
                resultIsHostActive = Boolean.valueOf("true");
            } else {
                resultIsHostActive = Boolean.valueOf("false");
            }
        } catch (Exception e) {
            log.info("{}class CheckDeviceIcmp\nmethod exec()\nException - {}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }

        if(this.getIsHostActive() == null || this.getIsHostActive() != resultIsHostActive ){
            log.info("{}Host state changed: \nHost- {}\nthis.getIsHostActive() - {}\nresultIsHostActive - {}{}",DATA_SEPARATOR,host,this.getIsHostActive(),resultIsHostActive,DATA_SEPARATOR);
            monitoringCheckResultDTO = MonitoringCheckResultDTO.builder()
                    .agentUUID(getAgentUUID())
                    .commandUUID(getCommandUUID())
                    .isAddNew(false)
                    .build();
            final List<Map<String,String>> result = new ArrayList<>();
            final Map<String,String> checkResult = new HashMap<>();
            checkResult.put("host", host);
            checkResult.put("isActive", String.valueOf(resultIsHostActive));
            result.add(checkResult);
            monitoringCheckResultDTO.setResult(result);
            this.setIsHostActive(resultIsHostActive);
        }
        else {
            log.info("{}Host is up to date: \nHost - {}\nthis.getIsHostActive() - {}\nresultIsHostActive - {}{}",DATA_SEPARATOR,host,this.getIsHostActive(),resultIsHostActive,DATA_SEPARATOR);
        }
        return monitoringCheckResultDTO;
    }

    @Override
    protected void stop() {

    }

//    @Override
//    protected CheckIcmpResultDTO exec() {
//        final String host = getProperties().get("host");
//        CheckIcmpResultDTO checkIcmpResultDTO = CheckIcmpResultDTO.builder()
//                .agentUUID(getAgentUUID())
//                .commandUUID(getCommandUUID())
//                .isAddNew(false)
//                .build();
//        final List<Map<String,String>> result = new ArrayList<>();
//        try {
//            if (StringUtils.isBlank(host)) {
//                throw new IllegalArgumentException("Host is undefined");
//            }
//            final Map<String,String> checkResult = new HashMap<>();
//            checkResult.put("host", host);
//            final InetAddress inet = InetAddress.getByName(host);
//            log.info("Sending Ping Request to {}", host);
//            if (inet.isReachable(5000)) {
//                checkResult.put("isActive", "true");
//            } else {
//                checkResult.put("isActive", "false");
//            }
//            result.add(checkResult);
//        } catch (Exception e) {
//            log.error(StringUtils.EMPTY, e);
//        }
//        checkIcmpResultDTO.setResult(result);
//        return checkIcmpResultDTO;
//    }

}
