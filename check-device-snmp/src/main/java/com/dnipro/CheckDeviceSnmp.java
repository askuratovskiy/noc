package com.dnipro;

import com.dnipro.model.monitoring.MonitoringCheckResultDTO;
import lombok.extern.slf4j.Slf4j;
import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.*;
import org.snmp4j.transport.DefaultUdpTransportMapping;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class CheckDeviceSnmp extends BaseCommand<MonitoringCheckResultDTO> implements Serializable {

    private static final String DATA_SEPARATOR = "\n----------------------------------------\n";

    @Override
    protected MonitoringCheckResultDTO exec() {
        log.info("{}class CheckDeviceSnmp\nCheckSnmpResultDTO\n",DATA_SEPARATOR);
        MonitoringCheckResultDTO monitoringCheckResultDTO = MonitoringCheckResultDTO.builder()
                .agentUUID(this.getAgentUUID())
                .commandUUID(this.getCommandUUID())
                .jobId(this.getJobId())
                .build();
        final String host = getProperties().get("host");
        final String oidValue = getProperties().get("oid");
        final String community = getProperties().get("community");
        String port = "161";
        int snmpVersion = SnmpConstants.version2c;

        TransportMapping transport = null;
        PDU pdu = null;
        Snmp snmp = null;
        ResponseEvent response = null;
        try {
            transport = new DefaultUdpTransportMapping();
            transport.listen();
            CommunityTarget comtarget = new CommunityTarget();
            comtarget.setCommunity(new OctetString(community));
            comtarget.setVersion(snmpVersion);
            comtarget.setAddress(new UdpAddress(host + "/" + port));
            comtarget.setRetries(2);
            comtarget.setTimeout(1000);
            pdu = new PDU();
            pdu.add(new VariableBinding(new OID(oidValue)));
            pdu.setType(PDU.GET);
            pdu.setRequestID(new Integer32(1));
            snmp = new Snmp(transport);
//
//            log.info("Sending Request to Agent...\nHost - {}\nIOD = {}",host,oidValue);
            response = snmp.get(pdu, comtarget);

            if (response != null) {
//                log.info("Got Response from Agent\nHost - {}\nIOD = {}",host,oidValue);
                PDU responsePDU = response.getResponse();
                if (responsePDU != null && responsePDU.getErrorStatus() == PDU.noError) {
                        final List<Map<String,String>> result = new ArrayList<>();
                        final Map<String,String> checkResult = new HashMap<>();
                        checkResult.put("host", host);
                        checkResult.put("oid",getProperties().get("oid"));
                        checkResult.put("getResult",responsePDU.get(0).toValueString());
                        result.add(checkResult);
                    monitoringCheckResultDTO.setResult(result);
                        log.info("SNMP GOT\nHost - {}\nIOD = {}\nResult value - {}{}",host,oidValue,responsePDU.get(0).toValueString());
                } else {
                    log.info("{}Error: Response PDU is null\nHost - {}\nIOD = {}{}", DATA_SEPARATOR,host,oidValue,DATA_SEPARATOR);
                }
            } else {
                log.info("{}Error: Agent Timeout...\nHost - {}\nIOD = {}{}", DATA_SEPARATOR,host,oidValue,DATA_SEPARATOR);
            }
            snmp.close();
        } catch (IOException e) {
            log.info("{}class CheckDeviceSnmp\nmethod exec()\nHost - {}\nIOD = {}\nException - {}{}", DATA_SEPARATOR,host,oidValue,e,DATA_SEPARATOR);
        }
        return  monitoringCheckResultDTO;
    }
    @Override
    protected void stop() {

    }
}
