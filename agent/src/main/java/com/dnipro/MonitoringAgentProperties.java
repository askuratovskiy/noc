package com.dnipro;

import lombok.*;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class MonitoringAgentProperties {
    private String agentName;
    private String instanceId;
    private Date lastModify;

}
