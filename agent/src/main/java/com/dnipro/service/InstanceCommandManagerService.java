package com.dnipro.service;

import com.dnipro.common.BaseCommand;
import com.dnipro.InstanceClassManagerInterface;
import com.dnipro.model.monitoring.MonitoringCommandDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@Slf4j
public class InstanceCommandManagerService extends ClassLoader implements InstanceClassManagerInterface<BaseCommand> {

    private static List<Map<String, String>> loadedCommand = new ArrayList<>();


    @Autowired
    private BaseCommand baseCommand;
    @Autowired
    private HttpService httpService;
    @Value("${application.server.url}")
    private String serverUrl;
    @Value("${application.server.path.config.getCommand}")
    private String serverPathGetCommand;

    @Override
    public BaseCommand getCommand(String commandUUID) {
        BaseCommand newCommand = null;
        MonitoringCommandDTO monitoringCommandDTO = checkInstance(commandUUID);
        if (Objects.nonNull(monitoringCommandDTO)) {
            try {
                newCommand = this.loadInstance(monitoringCommandDTO).newInstance();
            } catch (ClassNotFoundException e) {
                log.info("getCommand checkInstance ClassNotFoundException");
            } catch (Exception e) {
                log.info("getCommand checkInstance Exception");
            }
        } else {
            monitoringCommandDTO = getInstanceFromServer(commandUUID);
            try {
                newCommand = loadInstance(monitoringCommandDTO).newInstance();
                Map<String, String> command = new HashMap<>();
                command.put("commandUUID", monitoringCommandDTO.getUUID());
                command.put("commandName", monitoringCommandDTO.getName());
                command.put("commandPath", monitoringCommandDTO.getPath());
                loadedCommand.add(command);
            } catch (ClassNotFoundException e) {
                log.info("getCommand !checkInstance ClassNotFoundException");
            } catch (Exception e) {
                log.info("getCommand !checkInstance Exception");
            }
        }
        return newCommand;
    }

    private MonitoringCommandDTO checkInstance(String commandUUID) {
        MonitoringCommandDTO monitoringCommandDTO = null;
        final UUID newCommandUUID = UUID.fromString(commandUUID);
        for (Map<String, String> command : loadedCommand) {
            if (UUID.fromString(command.get("commandUUID")).equals(newCommandUUID)) {
                monitoringCommandDTO = MonitoringCommandDTO.builder()
                        .name(command.get("commandName"))
                        .UUID(command.get("commandUUID"))
                        .path(command.get("commandPath"))
                        .build();
            }
        }
        return monitoringCommandDTO;
    }

    private MonitoringCommandDTO getInstanceFromServer(String commandUUID) {
        final String configServerPathGetCommand = serverUrl + serverPathGetCommand;
        MonitoringCommandDTO monitoringCommandDTO = MonitoringCommandDTO.builder()
                .UUID(commandUUID)
                .build();
        monitoringCommandDTO = (MonitoringCommandDTO) httpService.postRequest(configServerPathGetCommand, monitoringCommandDTO, monitoringCommandDTO);
        return monitoringCommandDTO;
    }

    private Class<? extends BaseCommand> loadInstance(MonitoringCommandDTO monitoringCommandDTO) throws ClassNotFoundException {
        if (Objects.nonNull(monitoringCommandDTO.getByteArray())) {
            return (Class<? extends BaseCommand>) defineClass(monitoringCommandDTO.getPath(), monitoringCommandDTO.getByteArray(), 0, monitoringCommandDTO.getByteArray().length);
        }
        return (Class<? extends BaseCommand>) findLoadedClass(monitoringCommandDTO.getPath());
    }
}
