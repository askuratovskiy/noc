package com.dnipro.service;

import com.dnipro.common.BaseCommand;
import com.dnipro.MonitoringAgentProperties;
import com.dnipro.model.BaseDTO;
import com.dnipro.model.monitoring.MonitoringAgentPropertiesDTO;
import com.dnipro.model.server.ServerAgentJobDTO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;

import static java.lang.Thread.sleep;

@Service
@Slf4j
public class JobManagerService {

    private static final String DATA_SEPARATOR = "\n----------------------------------------\n";
    private static final long EVERY_MINUTE = 1000 * 60;


    @Autowired
    private MonitoringAgentProperties monitoringAgentProperties;
    @Autowired
    private HttpService httpService;
    @Autowired
    private ExecutorService executorService;
    @Autowired
    private InstanceCommandManagerService instanceCommandManagerService;
    @Autowired
    private CopyOnWriteArrayList<BaseCommand> jobs;

    @Value("${application.path.root}")
    private String appRoot;
    @Value("${application.server.url}")
    private String serverUrl;
    @Value("${application.server.path.config.getJob}")
    private String serverPathGetJob;
    @Value("${application.server.path.config.getCommand}")
    private String serverPathGetCommand;



    public List<BaseCommand> init() {
        return this.jobs;
    }

    public synchronized void addJob(BaseCommand command, Map<String, String> properties) {
        try {
//            final Class<?> job = this.getClass().getClassLoader().loadClass(properties.get("commandPath"));
//            final BaseCommand command = (BaseCommand) job.newInstance();
                command.setJobId(properties.get("jobId"));
                command.setResultConsumer(resultDTO -> httpService.sendResult(properties.get("serverResultUrl"), (BaseDTO) resultDTO));
                command.setProperties(properties);
                jobs.add(command);
                executorService.execute(command);
        } catch (Exception e) {
            log.error(StringUtils.EMPTY, e);
        }
    }

    public void removeJob(BaseCommand job) {
        job.close();
        jobs.forEach(e -> {
            if (e.equals(job))
                jobs.remove(e);
        });
    }

    public void updateJob(BaseCommand baseCommand, Map<String, String> newProperties) {
        baseCommand.close();
        baseCommand.setProperties(newProperties);
        baseCommand.setResultConsumer(resultDTO -> httpService.sendResult(newProperties.get("serverResultUrl"), (BaseDTO) resultDTO));
        baseCommand.run();
    }

    public void checkJob(BaseCommand command, Map<String, String> properties) {
        boolean isUpdate = false;
        boolean isActive = Boolean.valueOf(properties.get("active"));
        for (BaseCommand job : jobs) {
            if (job.getJobId().equals(properties.get("jobId"))) {
                if (isActive) {
                    updateJob(job, properties);
                    isUpdate = true;
                } else {
                    removeJob(job);
                }
            }
        }
        if (!isUpdate && isActive) {
            addJob(command, properties);
        }
    }

    public void checkLastModify(Date lastModify) {
        if (lastModify.compareTo(monitoringAgentProperties.getLastModify()) > 0) {
            monitoringAgentProperties.setLastModify(lastModify);
            log.info("{}lastJobUpdate is after monitoringAgentProperties.getLastJobUpdate()" +
                            "\nlastJobUpdate = {}" +
                            "\nmonitoringAgentProperties.getLastJobUpdate() = {}" +
                            "{}",
                    DATA_SEPARATOR,
                    lastModify.toString(),
                    monitoringAgentProperties.getLastModify().toString(),
                    DATA_SEPARATOR
            );
        }
    }


    //TODO: implement dynamic add job to BaseCommand
    @Scheduled(fixedRate = EVERY_MINUTE)
    public void getJobFromConfigServer() {
        final String configServerPathGetJob = serverUrl+serverPathGetJob;
        final HttpService httpService = new HttpService();
        final MonitoringAgentPropertiesDTO monitoringAgentPropertiesDTO = MonitoringAgentPropertiesDTO.builder()
                .instanceId(monitoringAgentProperties.getInstanceId())
                .agentName(monitoringAgentProperties.getAgentName())
                .lastUpdateDate(monitoringAgentProperties.getLastModify())
                .build();
        ServerAgentJobDTO serverAgentJobDTO = new ServerAgentJobDTO();
        serverAgentJobDTO = (ServerAgentJobDTO) httpService.postRequest(configServerPathGetJob,monitoringAgentPropertiesDTO, serverAgentJobDTO);
        //TODO: add timestamp to DB job
        if(!serverAgentJobDTO.getJobs().isEmpty()){
            log.info("{}ServerAgentJobDTO\nagentName = {}\ninstanceId = {}\njobs = {}\nCommandProperties = {}{}",
                    DATA_SEPARATOR,
                    serverAgentJobDTO.getAgentName(),
                    serverAgentJobDTO.getAgentInstanceId(),
                    serverAgentJobDTO.getJobs(),
                    serverAgentJobDTO.getCommandProperties(),
                    DATA_SEPARATOR
            );
            for (Map<String,String> job : serverAgentJobDTO.getJobs()) {
                BaseCommand command = instanceCommandManagerService.getCommand(job.get("commandUUID"));
                if (Objects.nonNull(command)) {
                    checkJob(command, job);
                    checkLastModify(new Date(Timestamp.valueOf(job.get("lastModify")).getTime()));
                } else {
                    log.info("{}NOT WORK TO AGENT{}", DATA_SEPARATOR, DATA_SEPARATOR);
                }
            }
        }
    }
}
