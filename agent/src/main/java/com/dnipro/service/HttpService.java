package com.dnipro.service;

import com.dnipro.model.BaseDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.util.Objects;

@Service
@Slf4j
public class HttpService {
    private static final String DATA_SEPARATOR = "\n----------------------------------------\n";


    public BaseDTO postRequest(String serverURL, BaseDTO requestDTO, BaseDTO responseDTO){
        BaseDTO baseDTO = responseDTO;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<? extends BaseDTO> response = null;
        try {
            HttpEntity<BaseDTO> request = new HttpEntity<>(requestDTO);
            response = restTemplate.postForEntity(serverURL, request, responseDTO.getClass());
            if(Objects.nonNull(response.getBody())){
                baseDTO = response.getBody();
            }
        } catch (ResourceAccessException e) {
            log.info("{}Error connect to config store\n Configuration didn't get{}", DATA_SEPARATOR, DATA_SEPARATOR);
        } catch (HttpServerErrorException e) {
            log.info("{}HttpServerErrorException\n Configuration didn't get{}", DATA_SEPARATOR, DATA_SEPARATOR);
        } catch (NullPointerException e) {
            log.info("{}Server response null DTO\n Configuration didn't get{}", DATA_SEPARATOR, DATA_SEPARATOR);
        }
        return baseDTO;
    }

    public <T extends BaseDTO> void sendResult(String serverResultUrl, T dto) {
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        try {
            final HttpEntity<T> request = new HttpEntity<>(dto, headers);
            final RestTemplate restTemplate = new RestTemplate();
            final ResponseEntity<String> response = restTemplate.postForEntity(serverResultUrl, request, String.class);
            log.info("{}CommandResultService postResultDTOtoServer response = {}{}", DATA_SEPARATOR, response, DATA_SEPARATOR);
        } catch (ResourceAccessException e) {
            log.info("{}Error connect to config store\n Result didn't send{}", DATA_SEPARATOR, DATA_SEPARATOR);
        } catch (Exception e) {
            log.info("\n-------------------------------\nCommandResultService postResultDTOtoServer Exception{}\n-------------------------------\n", e);
        }
    }
}
