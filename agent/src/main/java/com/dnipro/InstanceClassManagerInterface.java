package com.dnipro;

public interface InstanceClassManagerInterface<T>{
    T getCommand(String commandUUID);
}