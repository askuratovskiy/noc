package com.dnipro.config;

import com.dnipro.common.BaseCommand;
import com.dnipro.MonitoringAgentProperties;
import it.sauronsoftware.junique.AlreadyLockedException;
import it.sauronsoftware.junique.JUnique;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.DefaultManagedAwareThreadFactory;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Configuration
@Slf4j
@EnableScheduling
public class AppConfig {

    private static final String DATA_SEPARATOR = "\n----------------------------------------\n";

    @Value("${application.agent.instances}")
    private String[] instances;

    @Value("${application.agent.name}")
    private String agentName;

    private static final Resource[] RESOURCES = new ClassPathResource[]{
            new ClassPathResource("application.yml")
    };

    @Bean
    public static CopyOnWriteArrayList<BaseCommand> jobs() {
//        final List<BaseCommand> result = Collections.synchronizedList(new ArrayList<>());
        CopyOnWriteArrayList<BaseCommand> jobs = new CopyOnWriteArrayList<BaseCommand>();

        return jobs;
    }


    @Bean
    public static ExecutorService  executorService(){
        final ExecutorService executorService = Executors.newFixedThreadPool(100, new DefaultManagedAwareThreadFactory());
        return executorService;
    }



    @Bean
    public MonitoringAgentProperties monitoringAgent() {
        MonitoringAgentProperties result = null;
        for (String instance : instances) {
            log.info("{}Check instance - {}{}", DATA_SEPARATOR, instance, DATA_SEPARATOR);
            try {
                JUnique.acquireLock(instance);
                log.info("{}Free instance is found - {}\nApplication will run{}", DATA_SEPARATOR, instance, DATA_SEPARATOR);
                result = MonitoringAgentProperties.builder()
                        .instanceId(instance)
                        .agentName(agentName)
                        .lastModify(new Date(0))
                        .build();
                log.info("{}Init Bean monitoringAgent()\ninstanceId = {}\nagentName = {}\nlastJobUpdate = {}{}",
                        DATA_SEPARATOR,
                        result.getInstanceId(),
                        result.getAgentName(),
                        result.getLastModify(),
                        DATA_SEPARATOR
                );
                break;
            } catch (AlreadyLockedException e) {
                log.info("{}Instance is already running - {}{}", DATA_SEPARATOR, instance, DATA_SEPARATOR);
            }
        }
        if (Objects.isNull(result)) {
            log.info("{}System hasn't free instances, system halted{}", DATA_SEPARATOR, DATA_SEPARATOR);
            System.exit(3000);
        }
        return result;
    }
}
