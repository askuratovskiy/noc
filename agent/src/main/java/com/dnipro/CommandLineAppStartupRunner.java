package com.dnipro;

import com.dnipro.common.BaseCommand;
import com.dnipro.service.JobManagerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.concurrent.CopyOnWriteArrayList;

@Slf4j
@Component
public class CommandLineAppStartupRunner implements CommandLineRunner {
//    private static final ExecutorService EXECUTOR_SERVICE = Executors.newFixedThreadPool(100, new DefaultManagedAwareThreadFactory());

    @Autowired
    private JobManagerService jobManagerService;
    @Autowired
    private CopyOnWriteArrayList<BaseCommand> jobs;


    @Override
    public void run(String... args) throws Exception {
        jobManagerService.init();
        boolean isAnyActive = true;
        while (isAnyActive) {
            //TODO:
            isAnyActive = false;
            for (BaseCommand baseCommand : jobs) {
                if (baseCommand.isActive()) {
                    isAnyActive = true;
                    break;
                }
            }
            Thread.yield();
        }
        //executorService.shutdown();
        log.info("All services fished");
    }
}
