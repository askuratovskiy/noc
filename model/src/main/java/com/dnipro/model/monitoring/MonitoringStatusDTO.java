package com.dnipro.model.monitoring;

import com.dnipro.model.BaseDTO;
import lombok.*;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class MonitoringStatusDTO implements Serializable, BaseDTO {
    private String name;
    private String description;
}
