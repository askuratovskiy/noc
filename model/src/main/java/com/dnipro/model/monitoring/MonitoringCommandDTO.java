package com.dnipro.model.monitoring;

import com.dnipro.model.BaseDTO;
import lombok.*;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class MonitoringCommandDTO implements Serializable, BaseDTO {
    private String UUID;
    private String commandName;
    private String commandPath;
    private String jarName;
    private String jarResourcePath;
    private byte [] byteArray;
    private boolean active;
}
