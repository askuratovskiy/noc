package com.dnipro.model.monitoring;

import com.dnipro.model.BaseDTO;
import lombok.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class MonitoringCheckResultDTO implements Serializable, BaseDTO {
    private String jobId;
    private boolean isAddNew = false;
    private String agentUUID;
    private String commandName;
    private String commandUUID;
    private List<Map<String, String>> result = new ArrayList<>();
}
