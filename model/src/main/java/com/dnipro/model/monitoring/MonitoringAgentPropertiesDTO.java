package com.dnipro.model.monitoring;

import com.dnipro.model.BaseDTO;
import lombok.*;

import java.io.Serializable;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class MonitoringAgentPropertiesDTO implements Serializable, BaseDTO {
    private String UUID;
    private String name;
    private String instanceId;
    private String description;
    private Date lastUpdateDate;

}
