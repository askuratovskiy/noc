package com.dnipro.model.monitoring;

import com.dnipro.model.BaseDTO;
import lombok.*;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class MonitoringAgentJobDTO implements Serializable, BaseDTO {
    private Long id;
    private String commandName;
    private String checkInterval;
    private String host;
    private String status;
    private boolean active;
}
