package com.dnipro.model.server;

import com.dnipro.model.BaseDTO;
import lombok.Data;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

@Data
public class PostDTO  implements Serializable, BaseDTO {
    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    private Long id;
    private String title;
    private String url;
    private String date;

    public Date getSubmissionDateConverted(String timezone) throws ParseException {
        SIMPLE_DATE_FORMAT.setTimeZone(TimeZone.getTimeZone(timezone));
        return SIMPLE_DATE_FORMAT.parse(this.date);
    }

    public void setSubmissionDate(Date date, String timezone) {
        SIMPLE_DATE_FORMAT.setTimeZone(TimeZone.getTimeZone(timezone));
        this.date = SIMPLE_DATE_FORMAT.format(date);
    }
}