package com.dnipro.model.server;

import com.dnipro.model.BaseDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class DevicePropertiesDTO implements Serializable, BaseDTO {

    private Long id;
    private String ipAddress;
    private String hostName;
    private String serialNumber;
    private String macAddress;
    private String hardwareVersion;
    private String firmwareVersion;
}
