package com.dnipro.model.server;

import com.dnipro.model.BaseDTO;
import lombok.*;

import java.io.Serializable;
import java.util.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class MonitoringAgentJobDTO implements Serializable, BaseDTO {
    private String agentUUID;
    private Date agentLastModify;
    private List<Map<String, String>> jobs = new ArrayList<>();
}
