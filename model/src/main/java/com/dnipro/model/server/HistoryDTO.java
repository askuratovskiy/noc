package com.dnipro.model.server;

import com.dnipro.model.BaseDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class HistoryDTO implements Serializable, BaseDTO {
    private Long currentId;
    private Long newId;
    private String action;
    private String entityName;
    private String note;
    private Date lastTouched;
}
