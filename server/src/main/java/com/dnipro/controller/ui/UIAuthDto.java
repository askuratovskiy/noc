package com.dnipro.controller.ui;

import com.dnipro.model.BaseDTO;
import lombok.*;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class UIAuthDto implements Serializable, BaseDTO {
    private String user;
    private String password;
    private String checkInterval;
    private String host;
    private String status;
    private boolean active;
}