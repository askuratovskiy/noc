package com.dnipro.controller.monitoring;
import com.dnipro.model.monitoring.MonitoringAgentPropertiesDTO;
import com.dnipro.model.monitoring.MonitoringCheckResultDTO;
import com.dnipro.model.monitoring.MonitoringCommandDTO;
import com.dnipro.model.server.MonitoringAgentJobDTO;
import com.dnipro.service.monitoring.MonitoringManagerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("/api/monitoring")
public class MonitoringController {
    private static final String DATA_SEPARATOR = "\n----------------------------------------\n";

    @Autowired
    private MonitoringManagerService monitoringManagerService;

    @PostMapping(value = "/config/get-agent-properties", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<MonitoringAgentPropertiesDTO> getAgentProperties(@RequestBody MonitoringAgentPropertiesDTO monitoringAgentPropertiesDTO){
        monitoringAgentPropertiesDTO = monitoringManagerService.getMonitoringAgentProperties(monitoringAgentPropertiesDTO);
        return new ResponseEntity<>(monitoringAgentPropertiesDTO, HttpStatus.OK);
    }

    @PostMapping(value = "/config/get-job", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<MonitoringAgentJobDTO> getJob(@RequestBody MonitoringAgentJobDTO monitoringAgentJobDTO){
        //TODO: implement MonitoringAgentJobDTO
        monitoringAgentJobDTO = monitoringManagerService.getJobByAgentUUID(monitoringAgentJobDTO);
        return new ResponseEntity<>(monitoringAgentJobDTO, HttpStatus.OK);
    }

    @PostMapping(value = "/result/icmp-check", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity resultIcmpCheck(@RequestBody MonitoringCheckResultDTO monitoringCheckResultDTO) {
        try {
            monitoringManagerService.resultIcmpCheck(monitoringCheckResultDTO);
        }catch (Exception e){
            log.info("{}class MonitoringController\n method resultIcmpCheck()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "/result/snmp-get-model", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity resultSnmpGetModel(@RequestBody MonitoringCheckResultDTO monitoringCheckResultDTO) {
        try {
            monitoringManagerService.resultSnmpGetModel(monitoringCheckResultDTO);
        }catch (Exception e){
            log.info("{}class MonitoringController\n method resultSnmpGetModel()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "/result/snmp-get-sn", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity resultSnmpGetSn(@RequestBody MonitoringCheckResultDTO monitoringCheckResultDTO) {
        try {
            monitoringManagerService.resultSnmpGetSn(monitoringCheckResultDTO);
        }catch (Exception e){
            log.info("{}class MonitoringController\n method resultSnmpGetSn()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "/result/snmp-get-mac", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity resultSnmpGetMac(@RequestBody MonitoringCheckResultDTO monitoringCheckResultDTO) {
        try {
            monitoringManagerService.resultSnmpGetMac(monitoringCheckResultDTO);
        }catch (Exception e){
            log.info("{}class MonitoringController\n method resultSnmpGetMac()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }


    @PostMapping(value = "/config/get-command", produces = MediaType.APPLICATION_JSON_UTF8_VALUE, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<MonitoringCommandDTO> getCommand(@RequestBody MonitoringCommandDTO monitoringCommandDTO) {
        try {
            monitoringCommandDTO = monitoringManagerService.getMonitoringCommandDTOByCommandUUID(monitoringCommandDTO.getUUID());
        }catch (Exception e){
            log.info("{}class MonitoringController\n method getCommand()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return new ResponseEntity<>(monitoringCommandDTO, HttpStatus.OK);
    }
}
