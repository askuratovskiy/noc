package com.dnipro.config;


import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableScheduling
@EnableTransactionManagement
@ComponentScan(basePackages = {
        "com.dnipro",
        "com.dnipro.repository.*",
        "com.dnipro.service.*",
        "com.dnipro.entity.*",
        "com.dnipro.controller.*"
})
@EntityScan(basePackages = {
        "com.dnipro.entity"
})
@EnableJpaRepositories(basePackages = {
        "com.dnipro.repository"
})
public class AppConfig {

}
