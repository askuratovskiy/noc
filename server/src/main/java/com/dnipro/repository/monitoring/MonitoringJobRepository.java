package com.dnipro.repository.monitoring;

import com.dnipro.entity.monitoring.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Repository
@Transactional
public interface MonitoringJobRepository extends JpaRepository<MonitoringJobEntity, Long> {
    List<MonitoringJobEntity> getByAgentsJobs (MonitoringAgentEntity monitoringAgentEntity);
//    List<MonitoringJobEntity> getByMonitoringAgentEntitiesAndStatus (MonitoringAgentEntity monitoringAgentEntity, String status);
//    List<MonitoringJobEntity> findByStatusName (String statusName);
//    List<MonitoringJobEntity> findByMonitoringAgentEntitiesAndStatusNameAndMaster(MonitoringAgentEntity monitoringAgentEntity, String statusName, boolean master);
//    List<MonitoringJobEntity> findByMonitoringAgentEntitiesAndStatus (MonitoringAgentEntity monitoringAgentEntity, MonitoringStatusEntity monitoringStatusEntity);
    List<MonitoringJobEntity> findByAgentsJobsAndLastModifyAfter(MonitoringAgentEntity monitoringAgentEntity, Date createDateAfter);
    List<MonitoringJobEntity> findByCommand (MonitoringCommandEntity monitoringCommandEntity);
    List<MonitoringJobEntity> findByCommandTemplateAndActiveAndMaster (MonitoringCommandTemplateEntity monitoringCommandTemplateEntity, boolean isActive, boolean isMaster);
    List<MonitoringJobEntity> findByJobTemplateAndActiveAndMaster (MonitoringJobTemplateEntity monitoringJobTemplateEntity, boolean isActive, boolean isMaster);
}
