package com.dnipro.repository.monitoring;

import com.dnipro.entity.monitoring.MonitoringAgentEntity;
import com.dnipro.entity.monitoring.MonitoringCommandEntity;
import com.dnipro.entity.monitoring.MonitoringJobEntity;
import com.dnipro.entity.monitoring.MonitoringJobTemplateEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Repository
@Transactional
public interface MonitoringJobTemplateRepository extends JpaRepository<MonitoringJobTemplateEntity, Long> {
    MonitoringJobTemplateEntity getById(long id);
    MonitoringJobTemplateEntity getByTemplateNameAndActiveAndMaster(String temlateName, boolean isActive, boolean isMaster);
}
