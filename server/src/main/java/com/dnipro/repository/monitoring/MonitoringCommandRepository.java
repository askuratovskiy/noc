package com.dnipro.repository.monitoring;

import com.dnipro.entity.monitoring.MonitoringCommandEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface MonitoringCommandRepository extends JpaRepository<MonitoringCommandEntity, Long> {
    MonitoringCommandEntity getById(Long id);
    MonitoringCommandEntity getByCommandNameAndActiveAndMaster(String name, boolean isActive, boolean isMaster);
    MonitoringCommandEntity getByUuidAndMasterAndActive(String uuid, boolean isMaster, boolean isActive);
    MonitoringCommandEntity getByUuidAndMaster(String uuid, boolean isMaster);
}
