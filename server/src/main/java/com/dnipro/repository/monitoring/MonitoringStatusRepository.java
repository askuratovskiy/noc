package com.dnipro.repository.monitoring;

import com.dnipro.entity.monitoring.MonitoringStatusEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface MonitoringStatusRepository extends JpaRepository<MonitoringStatusEntity, Long> {

    MonitoringStatusEntity getByName(String statusName);

}
