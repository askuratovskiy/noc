package com.dnipro.repository.monitoring;

import com.dnipro.entity.monitoring.MonitoringAgentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface MonitoringAgentRepository extends JpaRepository<MonitoringAgentEntity, Long> {
    List<MonitoringAgentEntity> findByName(String name);
    MonitoringAgentEntity findByNameAndInstanceId(String name, String instanceId);
    MonitoringAgentEntity findByInstanceId(String instanceId);
    List<MonitoringAgentEntity> findByUuid(String uuid);
    MonitoringAgentEntity findByUuidAndMaster(String uuid, boolean isMaster);
}
