package com.dnipro.repository.monitoring;


import com.dnipro.entity.monitoring.MonitoringCommandEntity;
import com.dnipro.entity.monitoring.MonitoringCommandTemplateEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface MonitoringCommandTemplateRepository extends JpaRepository<MonitoringCommandTemplateEntity, Long> {

    MonitoringCommandTemplateEntity findByTemplateNameAndMasterAndActive(String name, boolean isMaster, boolean isActive);
    List<MonitoringCommandTemplateEntity> findByCommandAndMasterAndActive (MonitoringCommandEntity monitoringCommandEntity, boolean isMaster, boolean isActive);
}
