package com.dnipro.repository.store;

import com.dnipro.entity.store.DeviceModelEntity;
import com.dnipro.entity.store.DeviceVendorEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeviceModelEntityRepository extends JpaRepository<DeviceModelEntity, Long> {

    DeviceModelEntity findById(long id);
    List<DeviceModelEntity> findByModelAndMaster(String model, boolean isMaster);
    DeviceModelEntity findBySnmpIdentifyAndMaster(String model, boolean isMaster);
    List<DeviceModelEntity> findByVendorAndMaster(DeviceVendorEntity deviceVendorEntity, boolean isMaster);
}
