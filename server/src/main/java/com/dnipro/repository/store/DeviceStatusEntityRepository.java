package com.dnipro.repository.store;

import com.dnipro.entity.store.DeviceStatusEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeviceStatusEntityRepository extends JpaRepository<DeviceStatusEntity, Long> {
    DeviceStatusEntity findByStatusName(String statusName);
}
