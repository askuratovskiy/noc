package com.dnipro.repository.store;

import com.dnipro.entity.store.DeviceVendorEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeviceVendorEntityRepository extends JpaRepository<DeviceVendorEntity, Long> {
}
