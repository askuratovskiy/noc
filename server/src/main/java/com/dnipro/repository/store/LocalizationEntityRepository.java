package com.dnipro.repository.store;

import com.dnipro.entity.store.LocalizationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocalizationEntityRepository extends JpaRepository<LocalizationEntity, Long> {
}
