package com.dnipro.repository.store;

import com.dnipro.entity.store.DeviceMonitoringJobEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeviceMonitoringJobRepository extends JpaRepository<DeviceMonitoringJobEntity, Long> {
}
