package com.dnipro.repository.store;

import com.dnipro.entity.store.DevicePropertiesEntity;
import com.dnipro.entity.store.DeviceStatusEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface DevicePropertiesEntityRepository extends JpaRepository<DevicePropertiesEntity, Long> {

    DevicePropertiesEntity findById(long id);
    List<DevicePropertiesEntity> findByIpAddressAndMaster(String ipAddress, boolean isMaster);
    List<DevicePropertiesEntity> findByMacAddressAndMaster(String macAddress, boolean isMaster);
    List<DevicePropertiesEntity> findBySerialNumberAndMaster(String serialNumber, boolean isMaster);
    List<DevicePropertiesEntity> findByHostNameAndMaster(String hostName, boolean isMaster);
    List<DevicePropertiesEntity> findByIpAddressAndStatusAndMaster(String ipAddress, DeviceStatusEntity deviceStatusEntity, boolean isMaster);
}
