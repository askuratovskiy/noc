package com.dnipro.repository.store;

import com.dnipro.entity.store.DeviceTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeviceTypeEntityRepository extends JpaRepository<DeviceTypeEntity, Long> {
}
