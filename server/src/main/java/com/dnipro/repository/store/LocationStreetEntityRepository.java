package com.dnipro.repository.store;

import com.dnipro.entity.store.LocationStreetEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationStreetEntityRepository extends JpaRepository<LocationStreetEntity, Long> {
}
