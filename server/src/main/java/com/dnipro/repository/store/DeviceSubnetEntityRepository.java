package com.dnipro.repository.store;

import com.dnipro.entity.store.DeviceSubnetEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DeviceSubnetEntityRepository extends JpaRepository<DeviceSubnetEntity, Long> {
}
