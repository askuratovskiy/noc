package com.dnipro.repository.store;

import com.dnipro.entity.store.DeviceModelEntity;
import com.dnipro.entity.store.DeviceSnmpMibEntity;
import com.dnipro.entity.store.DeviceStatusEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeviceSnmpMibEntityRepository extends JpaRepository<DeviceSnmpMibEntity, Long> {

    DeviceSnmpMibEntity findById(long id);
    List<DeviceSnmpMibEntity> findByDescriptionAndMaster(String description, boolean isMaster);
    List<DeviceSnmpMibEntity> findBySnmpMibAndMaster(String mib, boolean isMaster);
    List<DeviceSnmpMibEntity> findBySnmpNameAndMaster(String name, boolean isMaster);
    List<DeviceSnmpMibEntity> findByModelAndMaster(DeviceModelEntity deviceModelEntity, boolean isMaster);
    List<DeviceSnmpMibEntity> findByStatusAndMaster(DeviceStatusEntity deviceStatusEntity, boolean isMaster);
}
