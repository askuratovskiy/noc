package com.dnipro.repository.store;

import com.dnipro.entity.store.LocationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface LocationEntityRepository extends JpaRepository<LocationEntity, Long> {
}
