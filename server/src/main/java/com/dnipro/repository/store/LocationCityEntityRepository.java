package com.dnipro.repository.store;

import com.dnipro.entity.store.LocationCityEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationCityEntityRepository extends JpaRepository<LocationCityEntity, Long> {
}
