package com.dnipro.repository.store;

import com.dnipro.entity.store.LocationDistrictEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LocationDistrictEntityRepository extends JpaRepository<LocationDistrictEntity, Long> {
}
