package com.dnipro.repository.store;

import com.dnipro.entity.store.DeviceEntity;
import com.dnipro.entity.store.DevicePropertiesEntity;
import com.dnipro.entity.store.DeviceStatusEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeviceEntityRepository extends JpaRepository<DeviceEntity, Long> {

    DeviceEntity findByProperties(DevicePropertiesEntity devicePropertiesEntity);
    DeviceEntity findByPropertiesAndStatus(DevicePropertiesEntity devicePropertiesEntity, DeviceStatusEntity deviceStatusEntity);
    List<DeviceEntity> findByPropertiesAndMaster(DevicePropertiesEntity devicePropertiesEntity, boolean isMaster);
    DeviceEntity findByActiveAndStatus(boolean active, DeviceStatusEntity deviceStatusEntity);
    DeviceEntity findById(long id);
    List<DeviceEntity> findByUuidAndActiveAndMaster(String uuid, boolean isActive, boolean isMaster);

}
