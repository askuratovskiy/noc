package com.dnipro.repository.store;

import com.dnipro.entity.store.HistoryActionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HistoryActionEntityRepository extends JpaRepository<HistoryActionEntity, Long> {
    HistoryActionEntity findByActionName(String actionName);
}
