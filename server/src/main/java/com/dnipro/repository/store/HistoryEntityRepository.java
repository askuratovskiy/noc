package com.dnipro.repository.store;

import com.dnipro.entity.store.HistoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface HistoryEntityRepository extends JpaRepository<HistoryEntity, Long> {
    List<HistoryEntity> findByEntityOldId(Long id);
    List<HistoryEntity> findByEntityNewId(Long id);
}
