package com.dnipro;

import com.dnipro.config.AppConfig;
import lombok.extern.slf4j.Slf4j;
import org.h2.tools.Server;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.sql.SQLException;

@Slf4j
@SpringBootApplication
public class RootApplication {
    public static void main(String[] args) throws SQLException {
        startH2Server();
        SpringApplication.run(RootApplication.class, args);
    }
    public static void startH2Server() throws SQLException {
        final Server embeddedH2TcpServer = Server.createTcpServer("-tcp", "-tcpAllowOthers", "-tcpPort", "9092");
        if (embeddedH2TcpServer.isRunning(true)) {
            log.info("H2 TCP store was started and is running.");
        } else {
            embeddedH2TcpServer.start();
            if (embeddedH2TcpServer.isRunning(true)) {
                log.info("H2 TCP store is successfully started.");
            } else {
                throw new SQLException("Could not start H2 store.");
            }
        }
    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }


    //TODO: https://www.baeldung.com/java-restart-spring-boot-app?utm_source=feedburner&utm_medium=feed&utm_campaign=Feed%3A+Baeldung+%28baeldung%29
//    private static ConfigurableApplicationContext context;
//
//    public static void main(String[] args) {
//        context = SpringApplication.run(RootApplication.class, args);
//    }
//
//    public static void restart() {
//        ApplicationArguments args = context.getBean(ApplicationArguments.class);
//
//
//        Thread thread = new Thread(() -> {
//            context.close();
//            context = SpringApplication.run(RootApplication.class, args.getSourceArgs());
//        });
//
//        thread.setDaemon(false);
//        thread.start();
//    }


}
