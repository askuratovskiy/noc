package com.dnipro.entity.store;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "location_street")
public class LocationStreetEntity {

    @Id
    @Column(name = "id", unique = true, updatable = false, insertable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "street_name", nullable = false, unique = true)
    private String streetName;

    @Column(name = "street_prefix", nullable = true, unique = false)
    private String streetPrefix;

    @Column(name = "street_old_name", nullable = true, unique = false)
    private String streetOldName;

    @Column(name = "street_alias", nullable = true, unique = false)
    private String streetAlias;

    @ManyToOne(targetEntity = LocalizationEntity.class)
    @JoinColumn(name = "localization", nullable = false, foreignKey = @ForeignKey(name = "localization_street_to_localization_id"))
    private LocalizationEntity localizationEntity;

    @Column(name = "master")
    private boolean master;
}
