package com.dnipro.entity.store;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "history_action")
@Getter
@Setter
public class HistoryActionEntity {

    @Id
    @Column(name = "id", unique = true, updatable = false, insertable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "action_name", nullable = false, unique = true)
    private String actionName;

    @Column(name = "description", nullable = true, unique = false)
    private String description;
}
