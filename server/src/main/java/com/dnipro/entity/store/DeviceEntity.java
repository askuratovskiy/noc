package com.dnipro.entity.store;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "device")
@Getter
@Setter
@EqualsAndHashCode
public class DeviceEntity {

    @Id
    @Column(name = "id", unique = true, updatable = false, insertable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "uuid", nullable = false, unique = false)
    private String uuid;

    @ManyToOne(targetEntity = DevicePropertiesEntity.class)
    @JoinColumn(name = "properties", foreignKey = @ForeignKey(name = "properties_device_to_device_properties_id"))
    private DevicePropertiesEntity properties;

    @ManyToOne(targetEntity = DeviceStatusEntity.class)
    @JoinColumn(name = "status", foreignKey = @ForeignKey(name = "status_device_to_device_status_id"))
    private DeviceStatusEntity status;

    @Column(name = "master")
    private boolean master;

    @Column(name = "active")
    private boolean active;
}
