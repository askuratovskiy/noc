package com.dnipro.entity.store;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "device_properties",
        indexes = {
                @Index(columnList = "id", name = "id"),
                @Index(columnList = "ip_address", name = "ip_address"),
                @Index(columnList = "master", name = "master")
        })
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class DevicePropertiesEntity {

    @Id
    @Column(name = "id", unique = true, updatable = false, insertable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "ip_address", nullable = true, unique = false)
    private String ipAddress;

    @Column(name = "hostname", nullable = true, unique = false)
    private String hostName;

    @Column(name = "serial_number", nullable = true, unique = false)
    private String serialNumber;

    @Column(name = "mac_address", nullable = true, unique = false)
    private String macAddress;

    @Column(name = "hardware_version", nullable = true, unique = false)
    private String hardwareVersion;

    @Column(name = "firmware_version", nullable = true, unique = false)
    private String firmwareVersion;

    @ManyToOne(targetEntity = DeviceModelEntity.class)
    @JoinColumn(name = "model", foreignKey = @ForeignKey(name = "model_device_property_to_device_model_id"))
    private DeviceModelEntity model;

    @ManyToOne(targetEntity = DeviceStatusEntity.class)
    @JoinColumn(name = "status", foreignKey = @ForeignKey(name = "status_device_properties_to_device_status_id"))
    private DeviceStatusEntity status;

    @Column(name = "master")
    private boolean master;
}
