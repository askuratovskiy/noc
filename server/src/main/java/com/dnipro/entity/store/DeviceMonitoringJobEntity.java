package com.dnipro.entity.store;

import com.dnipro.entity.monitoring.MonitoringJobEntity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "device_monitoring_job")
@Getter
@Setter
@EqualsAndHashCode
public class DeviceMonitoringJobEntity {

    @Id
    @Column(name = "id", unique = true, updatable = false, insertable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(targetEntity = DeviceEntity.class)
    @JoinColumn(name = "device_id", foreignKey = @ForeignKey(name = "DEVICE_MONITORING_JOB_TO_DEVICE_ID"))
    private DeviceEntity deviceId;

    @ManyToOne(targetEntity = MonitoringJobEntity.class)
    @JoinColumn(name = "monitoring_job_id", foreignKey = @ForeignKey(name = "DEVICE_MONITORING_JOB_TO_MONITORING_JOB_ID"))
    private MonitoringJobEntity monitoringJobEntity;

    @Column(name = "master")
    private boolean master;

    @Column(name = "active")
    private boolean active;

}
