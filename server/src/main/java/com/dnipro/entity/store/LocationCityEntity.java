package com.dnipro.entity.store;

import com.dnipro.entity.monitoring.MonitoringAgentEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "location_city")
public class LocationCityEntity {

    @Id
    @Column(name = "id", unique = true, updatable = false, insertable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "city_name", nullable = false, unique = true)
    private String cityName;

    @Column(name = "city_prefix", nullable = true, unique = false)
    private String cityPrefix;

    @Column(name = "city_old_name", nullable = true, unique = false)
    private String cityOldName;

    @Column(name = "city_alias", nullable = true, unique = false)
    private String cityAlias;

    @ManyToOne(targetEntity = LocalizationEntity.class)
    @JoinColumn(name = "localization", nullable = false, foreignKey = @ForeignKey(name = "localization_city_to_localization_id"))
    private LocalizationEntity localizationEntity;

    @ManyToMany(fetch = FetchType.LAZY,mappedBy = "city")
    private List<MonitoringAgentEntity> monitoringAgentEntities = new ArrayList<>();
    @Column(name = "master")
    private boolean master;

}
