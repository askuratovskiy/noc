package com.dnipro.entity.store;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "device_type")
@Getter
@Setter
@EqualsAndHashCode
public class DeviceTypeEntity {

    @Id
    @Column(name = "id", unique = true, updatable = false, insertable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "type_name", nullable = false, unique = true)
    private String typeName;

    @ManyToOne(targetEntity = DeviceStatusEntity.class)
    @JoinColumn(name = "status", foreignKey = @ForeignKey(name = "status_device_type_to_device_status_id"))
    private DeviceStatusEntity status;

    @Column(name = "master")
    private boolean master;

}
