package com.dnipro.entity.store;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "location")
public class LocationEntity {

    @Id
    @Column(name = "id", unique = true, updatable = false, insertable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(targetEntity = LocationCityEntity.class)
    @JoinColumn(name = "city", nullable = false, unique = false)
    private LocationCityEntity city;

    @ManyToOne(targetEntity = LocationDistrictEntity.class)
    @JoinColumn(name = "district", nullable = false, unique = false)
    private LocationDistrictEntity district;

    @ManyToOne(targetEntity = LocationStreetEntity.class)
    @JoinColumn(name = "street", nullable = false, unique = false)
    private LocationStreetEntity street;

    @Column(name = "house_number", nullable = true, unique = false)
    private int houseNumber;

    @Column(name = "house_suffix", nullable = true, unique = false)
    private String houseSuffix;

    @Column(name = "block", nullable = true, unique = false)
    private String block;

    @Column(name = "entrance_number", nullable = true, unique = false)
    private int entranceNumber;

    @Column(name = "floor", nullable = true, unique = false)
    private int floor;

    @Column(name = "technical_floor", nullable = true, unique = false)
    private boolean technicalFloor;

    @Column(name = "basement", nullable = false, unique = false)
    private boolean basement;

    @Column(name = "coordinate_latitude")
    private String coordinateLatitude;

    @Column(name = "coordinate_longitude")
    private String coordinateLongitude;

    @Column(name = "active", nullable = false, unique = false)
    private boolean active;

    @ManyToOne(targetEntity = LocationEntity.class)
    @JoinColumn(name = "device", nullable = true, unique = true)
    private DeviceEntity device;

    @Column(name = "master")
    private boolean master;
}
