package com.dnipro.entity.store;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "history")
public class HistoryEntity {

    @Id
    @Column(name = "id", unique = true, updatable = false, insertable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private Long id;

    @Getter
    @Setter
    @Column(name = "entity_old_id", nullable = false, unique = false)
    private Long entityOldId;

    @Getter
    @Setter
    @Column(name = "entity_new_id", nullable = false, unique = false)
    private Long entityNewId;

    @Getter
    @Setter
    @ManyToOne(targetEntity = HistoryActionEntity.class)
    @JoinColumn(name = "action", nullable = false, unique = false, foreignKey = @ForeignKey(name = "action_history_to_history_action_id"))
    private HistoryActionEntity action;

    @Getter
    @Setter
    @Column(name = "entity_name", nullable = false, unique = false)
    private String entityName;

    @Getter
    @Setter
    @Column(name = "note", nullable = true, unique = false)
    private String note;

    @Getter
    @Column(name = "timestamp", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date timestamp;

}
