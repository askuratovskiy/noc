package com.dnipro.entity.store;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "device_snmp_mib")
@Getter
@Setter
@EqualsAndHashCode
public class DeviceSnmpMibEntity {

    @Id
    @Column(name = "id", unique = true, updatable = false, insertable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "snmp_name", nullable = false)
    private String snmpName;

    @Column(name = "snmp_mib", nullable = false)
    private String snmpMib;

    @Column(name = "description")
    private String description;

    @ManyToOne(targetEntity = DeviceModelEntity.class)
    @JoinColumn(name = "device_model")
    private DeviceModelEntity model;

    @ManyToOne(targetEntity = DeviceStatusEntity.class)
    @JoinColumn(name = "status", foreignKey = @ForeignKey(name = "status_device_snmp_mib_to_device_status_id"))
    private DeviceStatusEntity status;

    @Column(name = "master")
    private boolean master;
}
