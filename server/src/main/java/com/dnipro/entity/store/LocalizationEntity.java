package com.dnipro.entity.store;



import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "localization")
@Getter
@Setter
public class LocalizationEntity {
    @Id
    @Column(name = "id", unique = true, updatable = false, insertable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "language", nullable = false, unique = true)
    private String language;
    @Column(name = "code", nullable = false, unique = true)
    private String code;
    @Column(name = "master")
    private boolean master;
}
