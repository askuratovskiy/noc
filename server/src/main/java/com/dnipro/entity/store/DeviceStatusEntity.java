package com.dnipro.entity.store;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "device_status")
@Getter
@Setter
@EqualsAndHashCode
public class DeviceStatusEntity {

    @Id
    @Column(name = "id", updatable = false, insertable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "status_name", nullable = false, unique = true)
    private String statusName;

    @Column(name = "description", nullable = true, unique = false)
    private String description;

}
