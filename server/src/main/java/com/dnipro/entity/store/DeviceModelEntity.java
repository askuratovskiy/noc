package com.dnipro.entity.store;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "device_model")
@Getter
@Setter
@EqualsAndHashCode
public class DeviceModelEntity {

    @Id
    @Column(name = "id", unique = true, updatable = false, insertable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(targetEntity = DeviceVendorEntity.class)
    @JoinColumn(name = "vendor", nullable = false, unique = false, foreignKey = @ForeignKey(name = "vendor_device_model_to_device_vendor_id"))
    private DeviceVendorEntity vendor;

    @Column(name = "model", nullable = false, unique = true)
    private String model;

    @Column(name = "snmp_identify", unique = true)
    private String snmpIdentify;

    @Column(name = "hardware_version", nullable = true, unique = false)
    private String hardwareVersion;

    @Column(name = "firmware_version", nullable = true, unique = false)
    private String firmwareVersion;

    @ManyToOne(targetEntity = DeviceStatusEntity.class)
    @JoinColumn(name = "status", foreignKey = @ForeignKey(name = "status_device_model_to_device_status_id"))
    private DeviceStatusEntity status;

    @ManyToOne(targetEntity = DeviceTypeEntity.class)
    @JoinColumn(name = "type", nullable = false, unique = false, foreignKey = @ForeignKey(name = "type_device_model_to_device_type_id"))
    private DeviceTypeEntity type;

    @Column(name = "master")
    private boolean master;

}
