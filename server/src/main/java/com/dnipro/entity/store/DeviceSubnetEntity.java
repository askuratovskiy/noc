package com.dnipro.entity.store;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "device_subnet")
@Getter
@Setter
@EqualsAndHashCode
public class DeviceSubnetEntity {

    @Id
    @Column(name = "id", unique = true, updatable = false, insertable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "subnet", nullable = false, unique = true)
    private String subnet;

    @Column(name = "gateway", nullable = false, unique = false)
    private String gateway;

    @ManyToOne(targetEntity = LocationCityEntity.class)
    @JoinColumn(name = "city", nullable = false, unique = false, foreignKey = @ForeignKey(name = "city_device_subnet_to_location_city_id"))
    private LocationCityEntity city;

    @ManyToOne(targetEntity = LocationDistrictEntity.class)
    @JoinColumn(name = "district", nullable = false, unique = false, foreignKey = @ForeignKey(name = "district_device_subnet_to_location_district_id"))
    private LocationDistrictEntity district;

    @ManyToOne(targetEntity = DeviceStatusEntity.class)
    @JoinColumn(name = "status", foreignKey = @ForeignKey(name = "status_device_subnet_to_device_status_id"))
    private DeviceStatusEntity status;

    @Column(name = "master")
    private boolean master;

}
