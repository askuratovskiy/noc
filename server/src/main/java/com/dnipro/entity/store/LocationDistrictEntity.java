package com.dnipro.entity.store;


import com.dnipro.entity.monitoring.MonitoringAgentEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "location_district")
public class LocationDistrictEntity {

    @Id
    @Column(name = "id", unique = true, updatable = false, insertable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "district_name", nullable = false, unique = true)
    private String districtName;

    @Column(name = "district_prefix", nullable = true, unique = false)
    private String districtPrefix;

    @Column(name = "district_old_name", nullable = true, unique = false)
    private String districtOldName;

    @Column(name = "district_alias", nullable = true, unique = false)
    private String districtAlias;

    @ManyToOne(targetEntity = LocalizationEntity.class)
    @JoinColumn(name = "localization", nullable = false, foreignKey = @ForeignKey(name = "localization_district_to_localization_id"))
    private LocalizationEntity localizationEntity;

    @ManyToMany(fetch = FetchType.LAZY, mappedBy = "district")
    private List<MonitoringAgentEntity> monitoringAgentEntities = new ArrayList<>();
    @Column(name = "master")
    private boolean master;
}
