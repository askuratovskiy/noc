package com.dnipro.entity.monitoring;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "monitoring_job_template")
public class MonitoringJobTemplateEntity {

    @Id
    @Column(name = "id", unique = true, updatable = false, insertable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private long id;

    @Column(nullable = false, unique = true, name = "template_name")
    @Getter
    @Setter
    private String templateName;

    @Column(nullable = false,name = "properties")
    @Getter
    @Setter
    private String properties;

    @Column(name = "master")
    @Getter
    @Setter
    private boolean master;

    @Column(name = "active")
    @Getter
    @Setter
    private boolean active;

}
