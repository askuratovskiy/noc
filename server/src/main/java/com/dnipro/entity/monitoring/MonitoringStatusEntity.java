package com.dnipro.entity.monitoring;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "monitoring_status")
public class MonitoringStatusEntity {

    @Id
    @Column(name = "id", unique = true, updatable = false, insertable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private Long id;

    @Getter
    @Setter
    @Column(name = "name", nullable = false, unique = true)
    private String name;

    @Getter
    @Setter
    @Column(name = "description", nullable = true, unique = false)
    private String description;

}
