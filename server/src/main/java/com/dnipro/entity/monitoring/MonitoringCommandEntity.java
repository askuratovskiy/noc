package com.dnipro.entity.monitoring;

import lombok.*;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "monitoring_command")
public class MonitoringCommandEntity {

    @Id
    @Column(name = "id", unique = true, updatable = false, insertable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private long id;

    @Column(name = "uuid",  nullable = false)
    @Getter
    @Setter
    private String uuid;

    @Column(name = "command_name",  nullable = false)
    @Getter
    @Setter
    private String commandName;

    @Column(name = "command_path", nullable = false)
    @Getter
    @Setter
    private String commandPath;

    @Column(name = "jar_name", nullable = false)
    @Getter
    @Setter
    private String jarName;

    @Column(name = "jar_path", nullable = false)
    @Getter
    @Setter
    private String jarPath;

    @Column(name = "active", nullable = false)
    @Getter
    @Setter
    private boolean active;

    @Column(name = "master", nullable = false)
    @Getter
    @Setter
    private boolean master;
}
