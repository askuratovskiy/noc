package com.dnipro.entity.monitoring;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "monitoring_command_template")
public class MonitoringCommandTemplateEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    long id;

    @Column(nullable = false, unique = true, name = "template_name")
    @Getter
    @Setter
    private String templateName;

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = MonitoringCommandEntity.class)
    @JoinColumn(nullable = false, name = "command", foreignKey = @ForeignKey(name = "COMMAND_MONITORING_COMMAND_TEMPLATE_TO_MONITORING_COMMAND_ID"))
    @Getter
    @Setter
    private MonitoringCommandEntity command;

    @Column(nullable = false,name = "properties")
    @Getter
    @Setter
    private String properties;

    @Column(name = "master")
    @Getter
    @Setter
    private boolean master;

    @Column(name = "active")
    @Getter
    @Setter
    private boolean active;

}
