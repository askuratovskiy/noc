package com.dnipro.entity.monitoring;

import com.dnipro.entity.store.LocationCityEntity;
import com.dnipro.entity.store.LocationDistrictEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

@Entity
@Getter
@Setter
@Table(name = "monitoring_agent")
public class MonitoringAgentEntity {


    @Id
    @Column(name = "id", unique = true, updatable = false, insertable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "uuid", nullable = false, updatable = false)
    private String uuid;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "instance_id", nullable = false)
    private String instanceId;

    @Column(name = "description")
    private String description;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
            name = "MONITORING_AGENT_TO_LOCATION_CITY",
            joinColumns = {@JoinColumn(name = "MONITORING_AGENT_ID")},
            inverseJoinColumns = {@JoinColumn(name = "LOCATION_CITY_ID")}
    )
    private List<LocationCityEntity> city = new ArrayList<>();


    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
            name = "MONITORING_AGENT_TO_LOCATION_DISTRICT",
            joinColumns = {@JoinColumn(name = "MONITORING_AGENT_ID")},
            inverseJoinColumns = {@JoinColumn(name = "LOCATION_DISTRICT_ID")}
    )
    private List<LocationDistrictEntity> district = new ArrayList<>();


    @Column(name = "active", nullable = false)
    private boolean active;

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = MonitoringStatusEntity.class)
    @JoinColumn(name = "status", foreignKey = @ForeignKey(name = "STATUS_MONITORING_AGENT_TO_MONITORING_STATUS_ID"))
    private MonitoringStatusEntity status;


//    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.ALL})
//    @JoinTable(
//            name = "MONITORING_AGENT_TO_MONITORING_JOB",
//            joinColumns = @JoinColumn(name = "MONITORING_AGENT_ID"),
//            inverseJoinColumns = @JoinColumn(name = "MONITORING_JOB_ID")
//    )
//    private List<MonitoringJobEntity> jobs = new ArrayList<>();

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
            name = "MONITORING_AGENT_TO_MONITORING_JOB",
            joinColumns = @JoinColumn(name = "MONITORING_AGENT_ID"),
            inverseJoinColumns = @JoinColumn(name = "MONITORING_JOB_ID")
    )
    private Set<MonitoringJobEntity> jobs = new HashSet<>();

    @Column(name = "master")
    private boolean master;

    public void addJob(MonitoringJobEntity monitoringJobEntity){
        jobs.add(monitoringJobEntity);
    }

}
