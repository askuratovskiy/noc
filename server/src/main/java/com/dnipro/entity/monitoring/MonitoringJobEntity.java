package com.dnipro.entity.monitoring;

import lombok.*;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "monitoring_job")
public class MonitoringJobEntity {

    @Id
    @Column(name = "id", unique = true, updatable = false, insertable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private long id;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.EAGER, targetEntity = MonitoringJobTemplateEntity.class)
    @JoinColumn(name = "job_template", foreignKey = @ForeignKey(name = "JOB_TEMPLATE_MONITORING_JOB_TO_MONITORING_JOB_TEMPLATE_ID"))
    private MonitoringJobTemplateEntity jobTemplate;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.EAGER, targetEntity = MonitoringCommandEntity.class)
    @JoinColumn(name = "command", foreignKey = @ForeignKey(name = "COMMAND_MONITORING_JOB_TO_MONITORING_COMMAND_ID"))
    private MonitoringCommandEntity command;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.EAGER, targetEntity = MonitoringCommandTemplateEntity.class)
    @JoinColumn(name = "command_template", foreignKey = @ForeignKey(name = "COMMAND_TEMPLATE_MONITORING_JOB_TO_MONITORING_COMMAND_TEMPLATE_ID"))
    private MonitoringCommandTemplateEntity commandTemplate;

    @Getter
    @Setter
    @Column(name = "host", nullable = false)
    private String host;

    @Getter
    @Setter
    @ManyToOne(fetch = FetchType.EAGER, targetEntity = MonitoringStatusEntity.class)
    @JoinColumn(name = "status", foreignKey = @ForeignKey(name = "STATUS_MONITORING_SERVICE_TO_MONITORING_STATUS_ID"))
    private MonitoringStatusEntity status;

    @Getter
    @Setter
    @Column(name = "active", nullable = false)
    private boolean active;

    @Getter
    @Setter
    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "jobs")
    private Set<MonitoringAgentEntity> agentsJobs = new HashSet<>();

    @Getter
    @Setter
    @Column(name = "master")
    private boolean master;

    @Getter
//    @Column(name = "last_modify", insertable = false, updatable = false,
//            columnDefinition = "TIMESTAMP default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP")
    @Column(name = "last_modify", insertable = false, updatable = false,
            columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModify;

}
