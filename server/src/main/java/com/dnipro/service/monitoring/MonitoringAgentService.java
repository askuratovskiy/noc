package com.dnipro.service.monitoring;

import com.dnipro.entity.monitoring.MonitoringAgentEntity;
import com.dnipro.entity.monitoring.MonitoringStatusEntity;;
import com.dnipro.repository.monitoring.MonitoringAgentRepository;
import com.dnipro.service.store.HistoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


@Slf4j
@Service
public class MonitoringAgentService {

    private static final String DATA_SEPARATOR = "\n----------------------------------------\n";

    @Autowired
    private MonitoringAgentRepository monitoringAgentRepository;
    @Autowired
    private MonitoringStatusService monitoringStatusService;
    @Autowired
    private HistoryService historyService;

    public void cloneMonitoringAgentEntity(MonitoringAgentEntity currentMonitoringAgentEntity){
        log.info("{}class MonitoringAgentService\n method cloneMonitoringAgentEntity()\n",DATA_SEPARATOR);
        MonitoringAgentEntity cloneMonitoringAgentEntity = new MonitoringAgentEntity();
        try {
            cloneMonitoringAgentEntity.setName(currentMonitoringAgentEntity.getName());
            cloneMonitoringAgentEntity.setInstanceId(currentMonitoringAgentEntity.getInstanceId());
            cloneMonitoringAgentEntity.setDescription(currentMonitoringAgentEntity.getDescription());
            cloneMonitoringAgentEntity.setUuid(currentMonitoringAgentEntity.getUuid());
            cloneMonitoringAgentEntity.setStatus(currentMonitoringAgentEntity.getStatus());
            cloneMonitoringAgentEntity.setJobs(currentMonitoringAgentEntity.getJobs());
            cloneMonitoringAgentEntity.setCity(currentMonitoringAgentEntity.getCity());
            cloneMonitoringAgentEntity.setDistrict(currentMonitoringAgentEntity.getDistrict());
            cloneMonitoringAgentEntity.setMaster(false);
            cloneMonitoringAgentEntity.setActive(currentMonitoringAgentEntity.isActive());
            monitoringAgentRepository.save(cloneMonitoringAgentEntity);
            historyService.addActionToHistory(MonitoringAgentEntity.class.getSimpleName(),"CLONE",currentMonitoringAgentEntity.getId(), cloneMonitoringAgentEntity.getId(),"cloneMonitoringAgentEntity");
            log.info("{}MonitoringAgentEntity cloned:\nId - {}\nName - {}{}",cloneMonitoringAgentEntity.getId(),cloneMonitoringAgentEntity.getName(),DATA_SEPARATOR);
        }catch (Exception e){
            log.info("{}class MonitoringAgentService\n method cloneMonitoringAgentEntity()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
    }

    public MonitoringAgentEntity createMonitoringAgentEntity(String name, String instanceId,String description, boolean isActive){
        log.info("{}class MonitoringAgentService\n method createMonitoringAgentEntity()\n",DATA_SEPARATOR);
        MonitoringAgentEntity monitoringAgentEntity = new MonitoringAgentEntity();
        try {
            monitoringAgentEntity.setUuid(UUID.fromString(name+instanceId).toString());
            monitoringAgentEntity.setName(name);
            monitoringAgentEntity.setInstanceId(instanceId);
            monitoringAgentEntity.setDescription(description);
            monitoringAgentEntity.setActive(isActive);
            monitoringAgentEntity.setMaster(true);
            MonitoringStatusEntity monitoringStatusEntity = monitoringStatusService.getMonitoringStatusEntityByName("CREATE");
            if(Objects.isNull(monitoringStatusEntity)){
                monitoringStatusEntity = monitoringStatusService.createMonitoringStatusEntity("CREATE","CREATE");
            }
            monitoringAgentEntity.setStatus(monitoringStatusEntity);
            monitoringAgentRepository.save(monitoringAgentEntity);
            historyService.addActionToHistory(MonitoringAgentEntity.class.getSimpleName(),"CREATE",monitoringAgentEntity.getId(), monitoringAgentEntity.getId(),"createMonitoringAgentEntity");
            log.info("{}MonitoringAgentEntity created:\nId - {}\nName - {}{}",monitoringAgentEntity.getId(),monitoringAgentEntity.getName(),DATA_SEPARATOR);
        }catch (Exception e){
            log.info("{}class MonitoringAgentService\n method createMonitoringAgentEntity()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return monitoringAgentEntity;
    }

    public MonitoringAgentEntity updateMonitoringAgentEntity(MonitoringAgentEntity currentMonitoringAgentEntity, String newName, String newInstanceId, String newDescription, String newStatusName, boolean newIsActive){
        log.info("{}class MonitoringAgentService\n method updateMonitoringAgentEntity()\n",DATA_SEPARATOR);
        cloneMonitoringAgentEntity(currentMonitoringAgentEntity);
        MonitoringAgentEntity monitoringAgentEntity = currentMonitoringAgentEntity;
        try {
            if(Objects.nonNull(newName)){
                monitoringAgentEntity.setName(newName);
            }
            if(Objects.nonNull(newInstanceId)){
                monitoringAgentEntity.setInstanceId(newInstanceId);
            }
            if(Objects.nonNull(newDescription)){
                monitoringAgentEntity.setDescription(newDescription);
            }

            if(monitoringAgentEntity.isActive()!=newIsActive){
                monitoringAgentEntity.setActive(newIsActive);
            }
            if(Objects.nonNull(newStatusName)){
                MonitoringStatusEntity monitoringStatusEntity = monitoringStatusService.getMonitoringStatusEntityByName(newStatusName);
                if(Objects.isNull(monitoringStatusEntity)){
                    monitoringStatusEntity = monitoringStatusService.createMonitoringStatusEntity(newStatusName,newStatusName);
                }
                monitoringAgentEntity.setStatus(monitoringStatusEntity);
            }
            monitoringAgentRepository.save(monitoringAgentEntity);
            historyService.addActionToHistory(MonitoringAgentEntity.class.getSimpleName(),"UPDATE",monitoringAgentEntity.getId(), monitoringAgentEntity.getId(),"updateMonitoringAgentEntity");
            log.info("{}MonitoringAgentEntity updated:\nId - {}\nName - {}{}",monitoringAgentEntity.getId(),monitoringAgentEntity.getName(),DATA_SEPARATOR);
        }catch (Exception e){
            log.info("{}class MonitoringAgentService\n method updateMonitoringAgentEntity()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return monitoringAgentEntity;
    }



    public void updateMonitoringAgentEntity(MonitoringAgentEntity currentMonitoringAgentEntity){

        log.info("{}class MonitoringAgentService\n method updateMonitoringAgentEntity()\n",DATA_SEPARATOR);
        try {
//        cloneMonitoringAgentEntity(currentMonitoringAgentEntity);
            monitoringAgentRepository.save(currentMonitoringAgentEntity);
            log.info("{}MonitoringAgentEntity updated:\nId - {}\nName - {}{}",currentMonitoringAgentEntity.getId(),currentMonitoringAgentEntity.getName(),DATA_SEPARATOR);
        }catch (Exception e){
            log.info("{}class MonitoringAgentService\n method updateMonitoringAgentEntity()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
    }

    public MonitoringAgentEntity getMonitoringAgentEntityById(Long id){
        log.info("{}class MonitoringAgentService\n method getMonitoringAgentEntityById()\n",DATA_SEPARATOR);
        MonitoringAgentEntity monitoringAgentEntity = null;
        try {
            monitoringAgentEntity = monitoringAgentRepository.getOne(id);
            log.info("{}MonitoringAgentEntity found:\nId - {}\nName - {}{}",monitoringAgentEntity.getId(),monitoringAgentEntity.getName(),DATA_SEPARATOR);
        }catch (Exception e){
            log.info("{}class MonitoringAgentService\n method getMonitoringAgentEntityById()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return monitoringAgentEntity;
    }
    public List<MonitoringAgentEntity> getMonitoringAgentEntitiesByName(String name){
        log.info("{}class MonitoringAgentService\n method getMonitoringAgentEntitiesByName()\n",DATA_SEPARATOR);
        List<MonitoringAgentEntity> monitoringAgentEntities = new ArrayList<>();
        try {
            monitoringAgentEntities = monitoringAgentRepository.findByName(name);
            log.info("{}MonitoringAgentEntity found{}",DATA_SEPARATOR);
        }
        catch (Exception e){
            log.info("{}class MonitoringAgentService\n method getMonitoringAgentEntitiesByName()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return monitoringAgentEntities;
    }

    public MonitoringAgentEntity getMonitoringAgentEntityByNameAndInstanceId(String name, String instanceId){
        log.info("{}class MonitoringAgentService\nmethod getMonitoringAgentEntityByNameAndInstanceId()\n",DATA_SEPARATOR);
        MonitoringAgentEntity monitoringAgentEntity = null;
        try {
            monitoringAgentEntity = monitoringAgentRepository.findByNameAndInstanceId(name, instanceId);
            log.info("\nMonitoringAgentEntity found:\nId - {}\nName - {}{}", monitoringAgentEntity.getId(), monitoringAgentEntity.getName(), DATA_SEPARATOR);
        }catch (NullPointerException e){
            log.info("MonitoringAgentEntity is't find:\nName - {}\nId - {}{}", name, instanceId, DATA_SEPARATOR);
        }catch (Exception e){
            log.info("{}class MonitoringAgentService\n method getMonitoringAgentEntityByNameAndInstanceId()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return monitoringAgentEntity;
    }

    public MonitoringAgentEntity getMonitoringAgentEntityByUUID(String uuid){
        log.info("{}class MonitoringAgentService\n method getMonitoringAgentEntityByUUID()\n",DATA_SEPARATOR);
        MonitoringAgentEntity monitoringAgentEntity = null;
        try {
            monitoringAgentEntity = monitoringAgentRepository.findByUuidAndMaster(uuid, true);
            log.info("MonitoringAgentEntity found:\nId - {}\nName - {}{}",monitoringAgentEntity.getId(),monitoringAgentEntity.getName(),DATA_SEPARATOR);
        }catch (Exception e){
            log.info("{}class MonitoringAgentService\n method getMonitoringAgentEntityByUUID()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return monitoringAgentEntity;
    }

    public boolean isMonitoringAgentEntityById(Long id){
        log.info("{}class MonitoringAgentService\n method isMonitoringAgentEntityById()\n",DATA_SEPARATOR);
        boolean result = false;
        try {
            MonitoringAgentEntity monitoringAgentEntity = monitoringAgentRepository.getOne(id);
            if(Objects.nonNull(monitoringAgentEntity)){
                result = true;
                log.info("{}MonitoringAgentEntity found:\nId - {}\nName - {}{}",monitoringAgentEntity.getId(),monitoringAgentEntity.getName(),DATA_SEPARATOR);
            }
            log.info("{}MonitoringAgentEntity were't find :\nId - {}{}",id,DATA_SEPARATOR);
        }catch (Exception e){
            log.info("{}class MonitoringAgentService\n method isMonitoringAgentEntityById()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return result;
    }

    public boolean isMonitoringAgentEntityByNameAndInstanceId(String name, String instanceId){
        log.info("{}class MonitoringAgentService\n method isMonitoringAgentEntityByNameAndInstanceId()\n",DATA_SEPARATOR);
        boolean result = false;
        try {
            MonitoringAgentEntity monitoringAgentEntity = monitoringAgentRepository.findByNameAndInstanceId(name,instanceId);
            if(Objects.nonNull(monitoringAgentEntity)){
                result = true;
                log.info("{}MonitoringAgentEntity found:\nId - {}\nName - {}{}",monitoringAgentEntity.getId(),monitoringAgentEntity.getName(),DATA_SEPARATOR);
            }
            log.info("{}MonitoringAgentEntity were't find :\nId - {}\nName - {}{}",name,instanceId,DATA_SEPARATOR);
        }catch (Exception e){
            log.info("{}class MonitoringAgentService\n method isMonitoringAgentEntityByNameAndInstanceId()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return result;
    }
}
