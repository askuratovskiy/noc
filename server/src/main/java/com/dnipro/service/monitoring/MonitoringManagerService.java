package com.dnipro.service.monitoring;

import com.dnipro.entity.monitoring.*;
import com.dnipro.entity.store.DeviceEntity;
import com.dnipro.entity.store.DeviceModelEntity;
import com.dnipro.entity.store.DevicePropertiesEntity;
import com.dnipro.entity.store.DeviceSnmpMibEntity;
import com.dnipro.model.monitoring.MonitoringAgentPropertiesDTO;
import com.dnipro.model.monitoring.MonitoringCheckResultDTO;
import com.dnipro.model.monitoring.MonitoringCommandDTO;
import com.dnipro.model.server.MonitoringAgentJobDTO;
import com.dnipro.repository.store.DeviceModelEntityRepository;
import com.dnipro.repository.store.DeviceSnmpMibEntityRepository;
import com.dnipro.service.store.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Service
@Slf4j
public class MonitoringManagerService {

    private static final String DATA_SEPARATOR = "\n----------------------------------------\n";


    @Autowired
    private HistoryService historyService;
    @Autowired
    private MonitoringCommandService monitoringCommandService;
    @Autowired
    private MonitoringAgentService monitoringAgentService;
    @Autowired
    private MonitoringJobService monitoringJobService;
    @Autowired
    private DeviceService deviceService;
    @Autowired
    private DevicePropertyService devicePropertyService;
    @Autowired
    private DeviceMonitoringJobService deviceMonitoringJobService;
    @Autowired
    private DeviceSnmpMibEntityRepository deviceSnmpMibEntityRepository;
    @Autowired
    private DeviceModelEntityRepository deviceModelEntityRepository;


    @Value("${application.server.url}")
    private String serverUrl;

//    @Scheduled(fixedRate = 1000 * 60)
//    private void createJob(){
//        log.info("{}class MonitoringManagerService method createJob{}", DATA_SEPARATOR,DATA_SEPARATOR);
//        MonitoringCommandEntity monitoringCommandEntity = monitoringCommandService.getMonitoringCommandEntityById(1L);
//        MonitoringCommandTemplateEntity monitoringCommandTemplateEntity = new MonitoringCommandTemplateEntity();
//        monitoringCommandTemplateEntity.setActive(true);
//        monitoringCommandTemplateEntity.setMaster(true);
//        monitoringCommandTemplateEntity.setTemplateName("CHECK_EVERY_MINUTE");
//        monitoringCommandTemplateEntity.setCommand(monitoringCommandEntity);
//        monitoringCommandTemplateEntity.setTemplateName(monitoringCommandEntity.getCommandName()+"_EVERY_MINUTE");
//        Map<String,String> properties = new HashMap<>();
//        properties.put("checkInterval","1");
//        properties.put("1","1");
//        properties.put("2","1");
//        properties.put("3","1");
//        monitoringCommandTemplateEntity.setProperties(properties.toString());
//        monitoringCommandService.addMonitoringCommandTemplateEntity(monitoringCommandTemplateEntity);
//    }

    public MonitoringAgentPropertiesDTO getMonitoringAgentProperties(MonitoringAgentPropertiesDTO monitoringAgentPropertiesDTO) {
        final String agentName = monitoringAgentPropertiesDTO.getName();
        final String agentInstanceId = monitoringAgentPropertiesDTO.getInstanceId();
        MonitoringAgentEntity monitoringAgentEntity = monitoringAgentService.getMonitoringAgentEntityByNameAndInstanceId(agentName, agentInstanceId);
        if (!Objects.isNull(monitoringAgentEntity)) {
            monitoringAgentPropertiesDTO.setUUID(monitoringAgentEntity.getUuid());
            monitoringAgentPropertiesDTO.setDescription(monitoringAgentEntity.getDescription());
        } else {
            monitoringAgentPropertiesDTO = null;
        }
        return monitoringAgentPropertiesDTO;
    }

    public MonitoringCommandDTO getMonitoringCommandDTOByCommandUUID(String commandUUID) {
        final MonitoringCommandEntity monitoringCommandEntity = monitoringCommandService.getMonitoringCommandEntityByUuidAndMasterAndActive(commandUUID, true, true);
        final byte[] jarCommandToByte = monitoringCommandService.getByteArrayByCommandJarFile(monitoringCommandEntity.getJarName());
        MonitoringCommandDTO monitoringCommandDTO = MonitoringCommandDTO.builder()
                .UUID(monitoringCommandEntity.getUuid())
                .commandName(monitoringCommandEntity.getCommandName())
                .commandPath(monitoringCommandEntity.getCommandPath())
                .jarName(monitoringCommandEntity.getJarName())
                .jarResourcePath(monitoringCommandEntity.getJarPath())
                .byteArray(jarCommandToByte)
                .active(monitoringCommandEntity.isActive())
                .build();
        return monitoringCommandDTO;
    }


    private Map<String, String> stringToMap(String string) {
        Map<String, String> map = new HashMap<>();
        string = string.substring(1, string.length() - 1);
        String[] keyValuePairs = string.split(",");
        for (String pair : keyValuePairs) {
            String[] entry = pair.split("=");
            map.put(entry[0].trim(), entry[1].trim());
        }
        return map;
    }

    public MonitoringAgentJobDTO getJobByAgentUUID(MonitoringAgentJobDTO monitoringAgentJobDTO) {
        try {
            MonitoringAgentEntity monitoringAgentEntity = monitoringAgentService.getMonitoringAgentEntityByUUID(monitoringAgentJobDTO.getAgentUUID());
            List<MonitoringJobEntity> monitoringAgentJobEntities = monitoringJobService.getMonitoringJobEntitiesByMonitoringAgentEntitiesAndCreateDateAfter(monitoringAgentEntity, monitoringAgentJobDTO.getAgentLastModify());
            List<Map<String, String>> jobs = new ArrayList<>();
            if (Objects.nonNull(monitoringAgentJobEntities)) {
                for (MonitoringJobEntity monitoringJobEntity : monitoringAgentJobEntities) {
                    final Map<String, String> commandProperties = stringToMap(monitoringJobEntity.getCommandTemplate().getProperties());
                    final Map<String, String> jobProperties = stringToMap(monitoringJobEntity.getJobTemplate().getProperties());
                    Map<String, String> job = new HashMap<>();
                    job.put("jobId", String.valueOf(monitoringJobEntity.getId()));
                    job.put("commandUUID", monitoringJobEntity.getCommand().getUuid());
                    job.put("host", monitoringJobEntity.getHost());
                    job.put("lastModify", monitoringJobEntity.getLastModify().toString());
                    job.put("active", String.valueOf(monitoringJobEntity.isActive()));
                    job.putAll(commandProperties);
                    job.putAll(jobProperties);
                    jobs.add(job);
                }
                monitoringAgentJobDTO.setJobs(jobs);
            }
        } catch (Exception e) {
        }
        return monitoringAgentJobDTO;
    }

//    public MonitoringAgentJobDTO getJobByAgentNameAndAgentInstanceId(MonitoringAgentPropertiesDTO monitoringAgentPropertiesDTO) {
//        MonitoringAgentJobDTO monitoringAgentJobDTO = null;
//        try {
//            MonitoringAgentEntity monitoringAgentEntity = monitoringAgentService.getMonitoringAgentEntityByNameAndInstanceId(monitoringAgentPropertiesDTO.getName(), monitoringAgentPropertiesDTO.getInstanceId());
//            List<MonitoringJobEntity> monitoringAgentJobEntities = monitoringJobService.getMonitoringJobEntitiesByMonitoringAgentEntitiesAndCreateDateAfter(monitoringAgentEntity, monitoringAgentPropertiesDTO.getLastUpdateDate());
//            List<Map<String, String>> jobs = new ArrayList<>();
//            if (Objects.nonNull(monitoringAgentJobEntities)) {
//                for (MonitoringJobEntity monitoringJobEntity : monitoringAgentJobEntities) {
//                    Map<String, String> job = new HashMap<>();
//                    job.put("jobId", String.valueOf(monitoringJobEntity.getId()));
//                    job.put("commandUUID", monitoringJobEntity.getCommand().getUuid());
//                    job.put("checkInterval", monitoringJobEntity.getCheckInterval());
//                    job.put("host", monitoringJobEntity.getHost());
//                    job.put("serverErrorUrl", monitoringJobEntity.getServerErrorUrl());
//                    job.put("serverResultUrl", monitoringJobEntity.getServerResultUrl());
//                    job.put("lastModify", monitoringJobEntity.getLastModify().toString());
//                    job.put("active", String.valueOf(monitoringJobEntity.isActive()));
//                    jobs.add(job);
//                }
//                monitoringAgentJobDTO = MonitoringAgentJobDTO.builder()
//                        .agentUUID(monitoringAgentEntity.getUuid())
//                        .agentName(monitoringAgentEntity.getName())
//                        .agentInstanceId(monitoringAgentEntity.getInstanceId())
//                        .jobs(jobs)
//                        .build();
//                Timestamp dd = new Timestamp(monitoringAgentPropertiesDTO.getLastUpdateDate().getTime());
//            } else {
//                Timestamp dd = new Timestamp(monitoringAgentPropertiesDTO.getLastUpdateDate().getTime());
//            }
//        }catch (Exception e){
//        }
//        return monitoringAgentJobDTO;
//    }

    private final Lock lockTrans = new ReentrantLock();

//    public void resultCheckCommand(MonitoringCheckResultDTO monitoringCheckResultDTO) {
//        String commandName = monitoringCommandService.getMonitoringCommandEntityByUUID(monitoringCheckResultDTO.getCommandUUID()).getCommandName();
//        for (Map<String, String> result : monitoringCheckResultDTO.getResult()) {
//            switch (commandName) {
//                case "CheckDeviceIcmp":
//                    resultIcmp(monitoringCheckResultDTO);
//                    break;
//                case "CheckSubnetIcmp":
//                    resultIcmp(monitoringCheckResultDTO);
//                    break;
//                case "CheckDeviceSnmp":
//                    resultIcmp(monitoringCheckResultDTO);
//                    break;
//            }
//        }

    public void resultSnmpGetSn(MonitoringCheckResultDTO monitoringCheckResultDTO) {
        for (Map<String, String> result : monitoringCheckResultDTO.getResult()) {
            final DevicePropertiesEntity devicePropertiesEntity = devicePropertyService.getForUpdateDevicePropertiesEntityByIpAddressAndMaster(result.get("host"), true).get(0);
            final String resultSerialNumber = result.get("getResult").toLowerCase();
            if(Objects.isNull(devicePropertiesEntity.getSerialNumber()) || !devicePropertiesEntity.getSerialNumber().equals(resultSerialNumber)) {
                devicePropertiesEntity.setSerialNumber(resultSerialNumber);
                devicePropertyService.updateDevicePropertiesEntity(devicePropertiesEntity);
            }
        }
    }

    public void resultSnmpGetMac(MonitoringCheckResultDTO monitoringCheckResultDTO) {
        for (Map<String, String> result : monitoringCheckResultDTO.getResult()) {
            final DevicePropertiesEntity devicePropertiesEntity = devicePropertyService.getForUpdateDevicePropertiesEntityByIpAddressAndMaster(result.get("host"), true).get(0);
            final String resultMac = result.get("getResult").replace("-", ":").toLowerCase();
            if(Objects.isNull(devicePropertiesEntity.getMacAddress())  || !devicePropertiesEntity.getMacAddress().equals(resultMac)) {
               devicePropertiesEntity.setMacAddress(resultMac);
                devicePropertyService.updateDevicePropertiesEntity(devicePropertiesEntity);
            }
        }
    }

    public void resultSnmpGetModel(MonitoringCheckResultDTO monitoringCheckResultDTO) {
        DeviceModelEntity deviceModelEntity = null;
        for (Map<String, String> result : monitoringCheckResultDTO.getResult()) {
            String [] resultArray = result.get("getResult").split(" ");
            for(String snmpIdentify : resultArray){
                deviceModelEntity = deviceModelEntityRepository.findBySnmpIdentifyAndMaster(snmpIdentify, true);
                if(Objects.nonNull(deviceModelEntity)){
                    break;
                }
            }
            DevicePropertiesEntity devicePropertiesEntity = devicePropertyService.getForUpdateDevicePropertiesEntityByIpAddressAndMaster(result.get("host"), true).get(0);
            if (Objects.nonNull(deviceModelEntity) && Objects.isNull(devicePropertiesEntity.getModel())  && !deviceModelEntity.equals(devicePropertiesEntity.getModel())) {
                devicePropertiesEntity.setModel(deviceModelEntity);
                devicePropertyService.updateDevicePropertiesEntity(devicePropertiesEntity);
            }
        }
    }

    private void createIcmpJobForDevice(String host, String agentUUID, DeviceEntity deviceEntity){
        MonitoringCommandEntity monitoringCommandEntity = monitoringCommandService.getMonitoringCommandEntityByNameAndMasterAndActive("CheckDeviceIcmp", true, true);
        MonitoringJobTemplateEntity monitoringJobTemplateEntity = monitoringJobService.getMonitoringJobTemplateEntityByNameAndActiveAndMaster("DEFAULT", true, true);
        MonitoringCommandTemplateEntity monitoringCommandTemplateEntity = monitoringCommandService.getMonitoringCommandTemplateEntityByNameAndIsMasterAndIsActive("CheckDeviceIcmp_DEFAULT", true, true);
        MonitoringJobEntity monitoringJobEntity = monitoringJobService.createMonitoringJobEntity(monitoringCommandEntity, host, monitoringJobTemplateEntity, monitoringCommandTemplateEntity, true);
        MonitoringAgentEntity monitoringAgentEntity = monitoringAgentService.getMonitoringAgentEntityByUUID(agentUUID);
        monitoringAgentEntity.addJob(monitoringJobEntity);
        monitoringAgentService.updateMonitoringAgentEntity(monitoringAgentEntity);
        deviceMonitoringJobService.createDeviceMonitoringJobEntity(monitoringJobEntity, deviceEntity);
    }

    private void createSnmpGetModelJobForDevice(String host, String agentUUID, DeviceEntity deviceEntity){
        MonitoringCommandEntity monitoringCommandEntity = monitoringCommandService.getMonitoringCommandEntityByNameAndMasterAndActive("CheckDeviceSnmp", true, true);
        MonitoringJobTemplateEntity monitoringJobTemplateEntity = monitoringJobService.getMonitoringJobTemplateEntityByNameAndActiveAndMaster("RESULT_SNMP_GET_MODEL", true, true);
        MonitoringCommandTemplateEntity monitoringCommandTemplateEntity = monitoringCommandService.getMonitoringCommandTemplateEntityByNameAndIsMasterAndIsActive("SNMP_GET_MODEL", true, true);
        MonitoringJobEntity monitoringJobEntity = monitoringJobService.createMonitoringJobEntity(monitoringCommandEntity, host, monitoringJobTemplateEntity, monitoringCommandTemplateEntity, true);
        MonitoringAgentEntity monitoringAgentEntity = monitoringAgentService.getMonitoringAgentEntityByUUID(agentUUID);
        monitoringAgentEntity.addJob(monitoringJobEntity);
        monitoringAgentService.updateMonitoringAgentEntity(monitoringAgentEntity);
        deviceMonitoringJobService.createDeviceMonitoringJobEntity(monitoringJobEntity, deviceEntity);
    }

    private void createSnmpGetMacJobForDevice(String host, String agentUUID, DeviceEntity deviceEntity){
        MonitoringCommandEntity monitoringCommandEntity = monitoringCommandService.getMonitoringCommandEntityByNameAndMasterAndActive("CheckDeviceSnmp", true, true);
        MonitoringJobTemplateEntity monitoringJobTemplateEntity = monitoringJobService.getMonitoringJobTemplateEntityByNameAndActiveAndMaster("RESULT_SNMP_GET_MAC", true, true);
        MonitoringCommandTemplateEntity monitoringCommandTemplateEntity = monitoringCommandService.getMonitoringCommandTemplateEntityByNameAndIsMasterAndIsActive("SNMP_GET_MAC_ADDRESS", true, true);
        MonitoringJobEntity monitoringJobEntity = monitoringJobService.createMonitoringJobEntity(monitoringCommandEntity, host, monitoringJobTemplateEntity, monitoringCommandTemplateEntity, true);
        MonitoringAgentEntity monitoringAgentEntity = monitoringAgentService.getMonitoringAgentEntityByUUID(agentUUID);
        monitoringAgentEntity.addJob(monitoringJobEntity);
        monitoringAgentService.updateMonitoringAgentEntity(monitoringAgentEntity);
        deviceMonitoringJobService.createDeviceMonitoringJobEntity(monitoringJobEntity, deviceEntity);
    }

    private void createSnmpGetSerialNumberJobForDevice(String host, String agentUUID, DeviceEntity deviceEntity){
        MonitoringCommandEntity monitoringCommandEntity = monitoringCommandService.getMonitoringCommandEntityByNameAndMasterAndActive("CheckDeviceSnmp", true, true);
        MonitoringJobTemplateEntity monitoringJobTemplateEntity = monitoringJobService.getMonitoringJobTemplateEntityByNameAndActiveAndMaster("RESULT_SNMP_GET_SN", true, true);
        MonitoringCommandTemplateEntity monitoringCommandTemplateEntity = monitoringCommandService.getMonitoringCommandTemplateEntityByNameAndIsMasterAndIsActive("SNMP_GET_SERIAL_NUMBER", true, true);
        MonitoringJobEntity monitoringJobEntity = monitoringJobService.createMonitoringJobEntity(monitoringCommandEntity, host, monitoringJobTemplateEntity, monitoringCommandTemplateEntity, true);
        MonitoringAgentEntity monitoringAgentEntity = monitoringAgentService.getMonitoringAgentEntityByUUID(agentUUID);
        monitoringAgentEntity.addJob(monitoringJobEntity);
        monitoringAgentService.updateMonitoringAgentEntity(monitoringAgentEntity);
        deviceMonitoringJobService.createDeviceMonitoringJobEntity(monitoringJobEntity, deviceEntity);
    }


    public void resultIcmpCheck(MonitoringCheckResultDTO monitoringCheckResultDTO) {
        for (Map<String, String> result : monitoringCheckResultDTO.getResult()) {
            final String host = result.get("host");
            final boolean isActiveHost = Boolean.parseBoolean(result.get("isActive"));
            if (monitoringCheckResultDTO.isAddNew() == true && isActiveHost == true) {
                log.info("{}May has a new host, new host will be add to DB\nisAddNew ={}{}", DATA_SEPARATOR, monitoringCheckResultDTO.isAddNew(), DATA_SEPARATOR);
                if (devicePropertyService.getDevicePropertiesEntityByIpAddressAndMaster(host, true).isEmpty()) {
                    DevicePropertiesEntity devicePropertiesEntity = devicePropertyService.createDevicePropertiesEntity();
                    devicePropertiesEntity.setIpAddress(host);
                    devicePropertyService.updateDevicePropertiesEntity(devicePropertiesEntity);
                    DeviceEntity deviceEntity = deviceService.createDeviceEntity();
                    deviceEntity.setProperties(devicePropertiesEntity);
                    deviceEntity.setActive(isActiveHost);
                    deviceService.updateDeviceEntity(deviceEntity);
//
                    createIcmpJobForDevice(host, monitoringCheckResultDTO.getAgentUUID(), deviceEntity);
                    createSnmpGetModelJobForDevice(host, monitoringCheckResultDTO.getAgentUUID(), deviceEntity);
                    createSnmpGetSerialNumberJobForDevice(host, monitoringCheckResultDTO.getAgentUUID(), deviceEntity);
                    createSnmpGetMacJobForDevice(host, monitoringCheckResultDTO.getAgentUUID(), deviceEntity);

                }
            } else {
                DevicePropertiesEntity devicePropertiesEntity = devicePropertyService.getDevicePropertiesEntityByIpAddressAndMaster(host, true).get(0);
                if (Objects.nonNull(devicePropertiesEntity)) {
                    final DeviceEntity deviceEntity = deviceService.getDeviceEntityByPropertyAndMaster(devicePropertiesEntity, true).get(0);
                    if (isActiveHost != deviceEntity.isActive()) {
                        deviceEntity.setActive(isActiveHost);
                        deviceService.updateDeviceEntity(deviceEntity);
                        log.info("isDevicePresent present: {}, changing active is - {}{}", DATA_SEPARATOR, host, isActiveHost, DATA_SEPARATOR);
                    }
                }
            }
        }
    }
}


