package com.dnipro.service.monitoring;

import com.dnipro.entity.monitoring.*;
import com.dnipro.repository.monitoring.MonitoringJobRepository;
import com.dnipro.repository.monitoring.MonitoringJobTemplateRepository;
import com.dnipro.service.store.HistoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;

@Service
@Slf4j
public class MonitoringJobService {
    private static final String DATA_SEPARATOR = "\n----------------------------------------\n";

    @Autowired
    private MonitoringJobRepository monitoringJobRepository;
    @Autowired
    private MonitoringJobTemplateRepository monitoringJobTemplateRepository;
    @Autowired
    private MonitoringStatusService monitoringStatusService;
    @Autowired
    private HistoryService historyService;


    public MonitoringJobTemplateEntity createMonitoringJobTemplateEntity(String templateName, boolean isActive, Map<String, String> properties){
        log.info("{}class MonitoringJobService\nmethod createMonitoringJobTemplateEntity()\n", DATA_SEPARATOR);
        MonitoringJobTemplateEntity monitoringJobTemplateEntity = null;
        try {
            monitoringJobTemplateEntity.setTemplateName(templateName);
            monitoringJobTemplateEntity.setActive(isActive);
            monitoringJobTemplateEntity.setMaster(true);
            monitoringJobTemplateEntity.setProperties(properties.toString());
            monitoringJobTemplateRepository.save(monitoringJobTemplateEntity);
            historyService.addActionToHistory(MonitoringJobTemplateEntity.class.getSimpleName(), "CREATE", monitoringJobTemplateEntity.getId(), monitoringJobTemplateEntity.getId(), "createMonitoringJobTemplateEntity");
        }catch (Exception e){
            log.info("{}class MonitoringJobService\n method createMonitoringJobTemplateEntity()\nException:\n{}{}", DATA_SEPARATOR, e, DATA_SEPARATOR);
        }
        return monitoringJobTemplateEntity;
    }

    public MonitoringJobTemplateEntity cloneMonitoringJobTemplateEntity(MonitoringJobTemplateEntity currentMonitoringJobTemplateEntity) {
        log.info("{}class MonitoringJobService\nmethod cloneMonitoringJobTemplateEntity()\n", DATA_SEPARATOR);
        MonitoringJobTemplateEntity cloneMonitoringJobTemplateEntity = new MonitoringJobTemplateEntity();
        try {
            cloneMonitoringJobTemplateEntity.setTemplateName(currentMonitoringJobTemplateEntity.getTemplateName());
            cloneMonitoringJobTemplateEntity.setActive(currentMonitoringJobTemplateEntity.isActive());
            cloneMonitoringJobTemplateEntity.setMaster(currentMonitoringJobTemplateEntity.isMaster());
            cloneMonitoringJobTemplateEntity.setProperties(currentMonitoringJobTemplateEntity.getProperties());
            monitoringJobTemplateRepository.save(cloneMonitoringJobTemplateEntity);
            historyService.addActionToHistory(MonitoringJobTemplateEntity.class.getSimpleName(), "CLONE", currentMonitoringJobTemplateEntity.getId(), cloneMonitoringJobTemplateEntity.getId(), "cloneMonitoringJobTemplateEntity");
            log.info("{}MonitoringJobTemplateEntity was cloned:\nId - {}\n{}", currentMonitoringJobTemplateEntity.getId(), DATA_SEPARATOR);
        }catch (Exception e){
            log.info("{}class MonitoringJobService\n method cloneMonitoringJobTemplateEntity()\nException:\n{}{}", DATA_SEPARATOR, e, DATA_SEPARATOR);
        }
        return cloneMonitoringJobTemplateEntity;
    }

    public MonitoringJobTemplateEntity updateMonitoringJobTemplateEntity(MonitoringJobTemplateEntity monitoringJobTemplateEntity){
        log.info("{}class MonitoringJobService\nmethod updateMonitoringJobTemplateEntity()\n", DATA_SEPARATOR);
        try {
            final MonitoringJobTemplateEntity cloneMonitoringJobTemplateEntity =  cloneMonitoringJobTemplateEntity(monitoringJobTemplateEntity);
            monitoringJobTemplateRepository.save(monitoringJobTemplateEntity);
            historyService.addActionToHistory(MonitoringJobTemplateEntity.class.getSimpleName(), "UPDATE", monitoringJobTemplateEntity.getId(), cloneMonitoringJobTemplateEntity.getId(), "updateMonitoringJobTemplateEntity");
            log.info("{}MonitoringJobTemplateEntity updated:\nId - {}\n{}", monitoringJobTemplateEntity.getId(), DATA_SEPARATOR);
        }catch (Exception e){
            log.info("{}class MonitoringJobService\n method updateMonitoringJobTemplateEntity()\nException:\n{}{}", DATA_SEPARATOR, e, DATA_SEPARATOR);
        }
        return monitoringJobTemplateEntity;
    }

    public MonitoringJobTemplateEntity getMonitoringJobTemplateEntityById(long id) {
        log.info("{}class MonitoringJobService\nmethod getMonitoringJobTemplateEntityById()\n", DATA_SEPARATOR);
        MonitoringJobTemplateEntity monitoringJobTemplateEntity = null;
        try {
            monitoringJobTemplateEntity = monitoringJobTemplateRepository.getById(id);
            log.info("{}MonitoringJobTemplateEntity found:\nId - {}\n{}", monitoringJobTemplateEntity.getId(), DATA_SEPARATOR);

        }catch (Exception e){
            log.info("{}class MonitoringJobService\n method getMonitoringJobTemplateEntityById()\nException:\n{}{}", DATA_SEPARATOR, e, DATA_SEPARATOR);
        }
        return monitoringJobTemplateEntity;
    }

    public MonitoringJobTemplateEntity getMonitoringJobTemplateEntityByNameAndActiveAndMaster(String templateName, boolean isActive, boolean isMaster) {
        log.info("{}class MonitoringJobService\nmethod getMonitoringJobTemplateEntityByNameAndActiveAndMaster()\n", DATA_SEPARATOR);
        MonitoringJobTemplateEntity monitoringJobTemplateEntity = null;
        try {
            monitoringJobTemplateEntity = monitoringJobTemplateRepository.getByTemplateNameAndActiveAndMaster(templateName, isActive, isMaster);
            log.info("{}MonitoringJobTemplateEntity found:\nId - {}\n{}", monitoringJobTemplateEntity.getId(), DATA_SEPARATOR);
        }catch (Exception e){
            log.info("{}class MonitoringJobService\n method getMonitoringJobTemplateEntityByNameAndActiveAndMaster()\nException:\n{}{}", DATA_SEPARATOR, e, DATA_SEPARATOR);
        }
        return monitoringJobTemplateEntity;
    }

    public MonitoringJobEntity createMonitoringJobEntity(MonitoringCommandEntity monitoringCommandEntity, String host, MonitoringJobTemplateEntity monitoringJobTemplateEntity, MonitoringCommandTemplateEntity monitoringCommandTemplateEntity, boolean isActive) {
        log.info("{}class MonitoringJobService\nmethod createMonitoringJobEntity()\n", DATA_SEPARATOR);
        MonitoringJobEntity monitoringJobEntity = new MonitoringJobEntity();
        try {
            monitoringJobEntity.setCommand(monitoringCommandEntity);
            monitoringJobEntity.setHost(host);
            MonitoringStatusEntity monitoringStatusEntity = monitoringStatusService.getMonitoringStatusEntityByName("CREATE");
            monitoringJobEntity.setJobTemplate(monitoringJobTemplateEntity);
            monitoringJobEntity.setCommandTemplate(monitoringCommandTemplateEntity);
            monitoringJobEntity.setStatus(monitoringStatusEntity);
            monitoringJobEntity.setActive(isActive);
            monitoringJobEntity.setMaster(true);
            monitoringJobRepository.save(monitoringJobEntity);
            historyService.addActionToHistory(MonitoringJobEntity.class.getSimpleName(), "CREATE", monitoringJobEntity.getId(), monitoringJobEntity.getId(), "createMonitoringJobEntity");
            log.info("{}MonitoringAgentEntity created:\nId - {}\n{}", monitoringJobEntity.getId(), DATA_SEPARATOR);
        } catch (Exception e) {
            log.info("{}class MonitoringJobService\n method createMonitoringJobEntity()\nException:\n{}{}", DATA_SEPARATOR, e, DATA_SEPARATOR);
        }
        return monitoringJobEntity;
    }

    public MonitoringJobEntity updateMonitoringJobEntity(MonitoringJobEntity currentMonitoringJobEntity){
        log.info("{}class MonitoringJobService\nmethod updateMonitoringJobEntity()\n", DATA_SEPARATOR);
        try{
            MonitoringJobEntity cloneMonitoringJobEntity = cloneMonitoringJobEntities(currentMonitoringJobEntity);
            monitoringJobRepository.save(currentMonitoringJobEntity);
            historyService.addActionToHistory(MonitoringJobEntity.class.getSimpleName(), "CREATE", currentMonitoringJobEntity.getId(), cloneMonitoringJobEntity.getId(), "updateMonitoringJobEntity");
        }catch (Exception e){
            log.info("{}class MonitoringJobService\nmethod updateMonitoringJobEntity()\nException:\n{}{}", DATA_SEPARATOR, e, DATA_SEPARATOR);
        }
        return currentMonitoringJobEntity;
    }

    public void updateTimeStampToMonitoringJobEntity(MonitoringJobEntity monitoringJobEntity) {
        log.info("{}class MonitoringJobService\nmethod createMonitoringJobEntity()\n", DATA_SEPARATOR);
        final MonitoringStatusEntity monitoringStatusEntityOne = monitoringStatusService.getMonitoringStatusEntityByName("UPDATE_TIMESTAMP");
        final MonitoringStatusEntity monitoringStatusEntityTwo = monitoringStatusService.getMonitoringStatusEntityByName("TIMESTAMP_UPDATE");
        try {
            if (monitoringJobEntity.getStatus().getName().equals(monitoringStatusEntityOne.getName())) {
                monitoringJobEntity.setStatus(monitoringStatusEntityTwo);
            }else {
                monitoringJobEntity.setStatus(monitoringStatusEntityOne);
            }
            monitoringJobRepository.save(monitoringJobEntity);
            historyService.addActionToHistory(MonitoringJobEntity.class.getSimpleName(), "UPDATE", monitoringJobEntity.getId(), monitoringJobEntity.getId(),"updateTimeStampToMonitoringJobEntity");
            log.info("{}MonitoringAgentEntity timestamp updated:\nId - {}\n{}", monitoringJobEntity.getId(), DATA_SEPARATOR);
        } catch (Exception e) {
            log.info("{}class MonitoringJobService\nmethod updateTimeStampToMonitoringJobEntity()\nException:\n{}{}", DATA_SEPARATOR, e, DATA_SEPARATOR);
        }
    }


    
    public void updateTimeStampToMonitoringJobEntities(List<MonitoringJobEntity> monitoringJobEntities) {
        log.info("{}class MonitoringJobService\nmethod updateTimeStampToMonitoringJobEntities()\n", DATA_SEPARATOR);
        for (MonitoringJobEntity monitoringJobEntity : monitoringJobEntities) {
            if (monitoringJobEntity.isActive()) {
                updateTimeStampToMonitoringJobEntity(monitoringJobEntity);
            }
        }
    }

    
    public List<MonitoringJobEntity> getMonitoringJobEntitiesByMonitoringAgentEntitiesAndCreateDateAfter(MonitoringAgentEntity monitoringAgentEntity, Date lastModify) {
        log.info("{}class MonitoringJobService\nmethod updateTimeStampToMonitoringJobEntities()\n", DATA_SEPARATOR);
        List<MonitoringJobEntity> monitoringJobEntities =  new ArrayList<>();
        try {
            monitoringJobEntities = monitoringJobRepository.findByAgentsJobsAndLastModifyAfter(monitoringAgentEntity, lastModify);
        }catch (Exception e){
            log.info("{}class MonitoringJobService\nmethod getMonitoringJobEntitiesByMonitoringAgentEntitiesAndCreateDateAfter()\nException:\n{}{}", DATA_SEPARATOR, e, DATA_SEPARATOR);
        }
        log.info("method updateTimeStampToMonitoringJobEntities(){}", DATA_SEPARATOR);
        return monitoringJobEntities;
    }

    public List<MonitoringJobEntity> getMonitoringJobEntitiesByMonitoringAgent(MonitoringAgentEntity monitoringAgentEntity) {
        log.info("{}class MonitoringJobService\nmethod updateTimeStampToMonitoringJobEntities()\n", DATA_SEPARATOR);
        List<MonitoringJobEntity> monitoringServiceEntities = null;
        try {
            monitoringServiceEntities = monitoringJobRepository.getByAgentsJobs(monitoringAgentEntity);
        }catch (Exception e){
            log.info("{}class MonitoringJobService\nmethod getMonitoringJobEntitiesByMonitoringAgent()\nException:\n{}{}", DATA_SEPARATOR, e, DATA_SEPARATOR);
        }
        return monitoringServiceEntities;
    }

    public MonitoringJobEntity getMonitoringJobEntitiesById(Long id) {
        log.info("{}class MonitoringJobService\nmethod getMonitoringJobEntitiesById()\n", DATA_SEPARATOR);
        MonitoringJobEntity monitoringJobEntity = null;
        try {
            monitoringJobEntity = monitoringJobRepository.getOne(id);
        }catch (Exception e){
            log.info("{}class MonitoringJobService\nmethod getMonitoringJobEntitiesById()\nException:\n{}{}", DATA_SEPARATOR, e, DATA_SEPARATOR);
        }
        return monitoringJobEntity;
    }

    
    public List<MonitoringJobEntity> getMonitoringJobEntitiesByCommand(MonitoringCommandEntity monitoringCommandEntity) {
        log.info("{}class MonitoringJobService\nmethod getMonitoringJobEntitiesByCommand()\n", DATA_SEPARATOR);
        List<MonitoringJobEntity> monitoringServiceEntities = null;
        try {
            monitoringServiceEntities = monitoringJobRepository.findByCommand(monitoringCommandEntity);
        }catch (Exception e){
            log.info("{}class MonitoringJobService\nmethod getMonitoringJobEntitiesByCommand()\nException:\n{}{}", DATA_SEPARATOR, e, DATA_SEPARATOR);
        }
        return monitoringServiceEntities;
    }

    public List<MonitoringJobEntity> getMonitoringJobEntitiesByCommandTemplateAndActiveAndMaster(MonitoringCommandTemplateEntity monitoringCommandTemplateEntity, boolean isActive, boolean isMaster) {
        log.info("{}class MonitoringJobService\nmethod getMonitoringJobEntitiesByCommandTemplateAndActiveAndMaster()\n", DATA_SEPARATOR);
        List<MonitoringJobEntity> monitoringServiceEntities = null;
        try {
            monitoringServiceEntities = monitoringJobRepository.findByCommandTemplateAndActiveAndMaster(monitoringCommandTemplateEntity, isActive ,isMaster);
        }catch (Exception e){
            log.info("{}class MonitoringJobService\nmethod getMonitoringJobEntitiesByCommandTemplateAndActiveAndMaster()\nException:\n{}{}", DATA_SEPARATOR, e, DATA_SEPARATOR);
        }
        return monitoringServiceEntities;
    }

    public List<MonitoringJobEntity> getMonitoringJobEntitiesByJobTemplateAndActiveAndMaster(MonitoringJobTemplateEntity monitoringJobTemplateEntity, boolean isActive, boolean isMaster) {
        log.info("{}class MonitoringJobService\nmethod getMonitoringJobEntitiesByJobTemplateAndActiveAndMaster()\n", DATA_SEPARATOR);
        List<MonitoringJobEntity> monitoringServiceEntities = null;
        try {
            monitoringServiceEntities = monitoringJobRepository.findByJobTemplateAndActiveAndMaster(monitoringJobTemplateEntity, isActive ,isMaster);
        }catch (Exception e){
            log.info("{}class MonitoringJobService\nmethod getMonitoringJobEntitiesByJobTemplateAndActiveAndMaster()\nException:\n{}{}", DATA_SEPARATOR, e, DATA_SEPARATOR);
        }
        return monitoringServiceEntities;
    }

    
    public MonitoringJobEntity cloneMonitoringJobEntities(MonitoringJobEntity currentMonitoringJobEntity) {
        log.info("{}class MonitoringJobService\nmethod cloneMonitoringJobEntities()\n", DATA_SEPARATOR);
        MonitoringJobEntity cloneMonitoringJobEntity = new MonitoringJobEntity();
        try {
            cloneMonitoringJobEntity.setCommand(currentMonitoringJobEntity.getCommand());
            cloneMonitoringJobEntity.setJobTemplate(currentMonitoringJobEntity.getJobTemplate());
            cloneMonitoringJobEntity.setCommandTemplate(currentMonitoringJobEntity.getCommandTemplate());
            cloneMonitoringJobEntity.setHost(currentMonitoringJobEntity.getHost());
            cloneMonitoringJobEntity.setStatus(currentMonitoringJobEntity.getStatus());
            cloneMonitoringJobEntity.setActive(currentMonitoringJobEntity.isActive());
            cloneMonitoringJobEntity.setMaster(false);
            monitoringJobRepository.save(cloneMonitoringJobEntity);
            historyService.addActionToHistory(MonitoringJobEntity.class.getSimpleName(), "CLONE", currentMonitoringJobEntity.getId(), cloneMonitoringJobEntity.getId(),"cloneMonitoringJobEntities");
            log.info("{}MonitoringAgentEntity cloned:\ncurrentId - {}\nnewId - {}{}", currentMonitoringJobEntity.getId(), cloneMonitoringJobEntity.getId(), DATA_SEPARATOR);
        }catch (Exception e){
            log.info("{}class MonitoringJobService\nmethod cloneMonitoringJobEntities()\nException:\n{}{}", DATA_SEPARATOR, e, DATA_SEPARATOR);
        }
        return cloneMonitoringJobEntity;
    }


}
