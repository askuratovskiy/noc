package com.dnipro.service.monitoring;

import com.dnipro.entity.monitoring.MonitoringCommandEntity;

import com.dnipro.entity.monitoring.MonitoringCommandTemplateEntity;
import com.dnipro.entity.monitoring.MonitoringStatusEntity;
import com.dnipro.model.monitoring.MonitoringCommandDTO;
import com.dnipro.repository.monitoring.MonitoringCommandRepository;
import com.dnipro.repository.monitoring.MonitoringCommandTemplateRepository;
import com.dnipro.service.store.HistoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.*;
import java.util.*;
import java.util.jar.JarFile;


@Service
@Slf4j
public class MonitoringCommandService {

    private static final long EVERY_MINUTE = 1000 * 60;

    private static final String DATA_SEPARATOR = "\n----------------------------------------\n";

    @Autowired
    private MonitoringCommandRepository monitoringCommandRepository;
    @Autowired
    private MonitoringCommandTemplateRepository monitoringCommandTemplateRepository;
    @Autowired
    private MonitoringJobService monitoringJobService;
    @Autowired
    private HistoryService historyService;

    @Value("${application.path.commands}")
    private String pathCommands;

    public void cloneMonitoringCommandTemplateEntity(MonitoringCommandTemplateEntity monitoringCommandTemplateEntity){
        log.info("{}class MonitoringCommandService method cloneMonitoringCommandTemplateEntity\n",DATA_SEPARATOR);
        MonitoringCommandTemplateEntity cloneMonitoringCommandTemplateEntity = new MonitoringCommandTemplateEntity();
        cloneMonitoringCommandTemplateEntity.setProperties(monitoringCommandTemplateEntity.getProperties());
        cloneMonitoringCommandTemplateEntity.setActive(monitoringCommandTemplateEntity.isActive());
        cloneMonitoringCommandTemplateEntity.setMaster(false);
        cloneMonitoringCommandTemplateEntity.setCommand(monitoringCommandTemplateEntity.getCommand());
        cloneMonitoringCommandTemplateEntity.setTemplateName(monitoringCommandTemplateEntity.getTemplateName());
        monitoringCommandTemplateRepository.save(cloneMonitoringCommandTemplateEntity);
        historyService.addActionToHistory(MonitoringCommandTemplateEntity.class.getSimpleName(), "CLONE", monitoringCommandTemplateEntity.getId(), cloneMonitoringCommandTemplateEntity.getId(), "cloneMonitoringCommandTemplateEntity");
        log.info("MonitoringCommandTemplateEntity was cloned ID - {}{}",cloneMonitoringCommandTemplateEntity.getId(),DATA_SEPARATOR);
    }

    public MonitoringCommandTemplateEntity addMonitoringCommandTemplateEntity(MonitoringCommandTemplateEntity monitoringCommandTemplateEntity){
        log.info("{}class MonitoringCommandService method addMonitoringCommandTemplateEntity\n",DATA_SEPARATOR);
        try {
            monitoringCommandTemplateRepository.save(monitoringCommandTemplateEntity);
            log.info("MonitoringCommandTemplateEntity was addes  ID - {}{}", monitoringCommandTemplateEntity.getId(),DATA_SEPARATOR);
            historyService.addActionToHistory(MonitoringCommandTemplateEntity.class.getSimpleName(), "UPDATE", monitoringCommandTemplateEntity.getId(), monitoringCommandTemplateEntity.getId(), "updateMonitoringCommandTemplateEntity");
        }catch (Exception e){
            log.info("{}class MonitoringCommandService method addMonitoringCommandTemplateEntity\nException - {}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return monitoringCommandTemplateEntity;
    }

    public MonitoringCommandTemplateEntity updateMonitoringCommandTemplateEntity(MonitoringCommandTemplateEntity monitoringCommandTemplateEntity){
        log.info("{}class MonitoringCommandService method updateMonitoringCommandTemplateEntity\n",DATA_SEPARATOR);
        try {
            cloneMonitoringCommandTemplateEntity(monitoringCommandTemplateEntity);
            monitoringCommandTemplateRepository.save(monitoringCommandTemplateEntity);
            log.info("MonitoringCommandTemplateEntity was cloned and updated  ID - {}{}", monitoringCommandTemplateEntity.getId(),DATA_SEPARATOR);
            historyService.addActionToHistory(MonitoringCommandTemplateEntity.class.getSimpleName(), "UPDATE", monitoringCommandTemplateEntity.getId(), monitoringCommandTemplateEntity.getId(), "updateMonitoringCommandTemplateEntity");
        }catch (Exception e){
            log.info("{}class MonitoringCommandService method updateMonitoringCommandTemplateEntity\nException - {}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return monitoringCommandTemplateEntity;
    }

    public MonitoringCommandTemplateEntity createMonitoringCommandTemplateEntity(String templateName, boolean isActive, Map<String,String> properties, MonitoringCommandEntity monitoringCommandEntity){
        log.info("{}class MonitoringCommandService method createMonitoringCommandTemplateEntity\n",DATA_SEPARATOR);
        MonitoringCommandTemplateEntity monitoringCommandTemplateEntity = new MonitoringCommandTemplateEntity();
        try {
            monitoringCommandTemplateEntity.setTemplateName(templateName);
            monitoringCommandTemplateEntity.setCommand(monitoringCommandEntity);
            monitoringCommandTemplateEntity.setMaster(true);
            monitoringCommandTemplateEntity.setActive(isActive);
            monitoringCommandTemplateEntity.setProperties(properties.toString());
            monitoringCommandTemplateRepository.save(monitoringCommandTemplateEntity);
            historyService.addActionToHistory(MonitoringCommandTemplateEntity.class.getSimpleName(), "CREATE", monitoringCommandTemplateEntity.getId(), monitoringCommandTemplateEntity.getId(), "createMonitoringCommandTemplateEntity");
            log.info("MonitoringCommandTemplateEntity was created ID - {}{}",monitoringCommandTemplateEntity.getId(),DATA_SEPARATOR);
        }catch (Exception e){
            log.info("{}class MonitoringCommandService method createMonitoringCommandTemplateEntity\nException - {}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return monitoringCommandTemplateEntity;
    }

    public MonitoringCommandTemplateEntity getMonitoringCommandTemplateEntityById(Long id){
        log.info("{}class MonitoringCommandService method getMonitoringCommandTemplateEntityById\n",DATA_SEPARATOR);
        MonitoringCommandTemplateEntity monitoringCommandTemplateEntity = null;
        try {
            monitoringCommandTemplateEntity = monitoringCommandTemplateRepository.getOne(id);
            log.info("MonitoringCommandTemplateEntity was found\nID - {}{}",monitoringCommandTemplateEntity.getId(),DATA_SEPARATOR);
        }catch (Exception e){
            log.info("{}class MonitoringCommandService method getMonitoringCommandTemplateEntityById\nException - {}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return monitoringCommandTemplateEntity;
    }

    public MonitoringCommandTemplateEntity getMonitoringCommandTemplateEntityByNameAndIsMasterAndIsActive(String name, boolean isMaster, boolean isActive){
        log.info("{}class MonitoringCommandService method getMonitoringCommandTemplateEntityByNameAndIsMasterAndIsActive\n",DATA_SEPARATOR);
        MonitoringCommandTemplateEntity monitoringCommandTemplateEntity = null;
        try {
            monitoringCommandTemplateEntity = monitoringCommandTemplateRepository.findByTemplateNameAndMasterAndActive(name, isMaster, isActive);
            log.info("MonitoringCommandTemplateEntity was found\nID - {}{}",monitoringCommandTemplateEntity.getId(),DATA_SEPARATOR);
        }catch (Exception e){
            log.info("{}class MonitoringCommandService method getMonitoringCommandTemplateEntityByNameAndIsMasterAndIsActive\nException - {}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return monitoringCommandTemplateEntity;
    }

    public List<MonitoringCommandTemplateEntity> getMonitoringCommandTemplateEntityByCommandAndIsMasterAndIsActive(MonitoringCommandEntity monitoringCommandEntity, boolean isMaster, boolean isActive){
        log.info("{}class MonitoringCommandService method getMonitoringCommandTemplateEntityByCommandAndIsMasterAndIsActive\n",DATA_SEPARATOR);
        List<MonitoringCommandTemplateEntity> monitoringCommandTemplateEntities =new ArrayList<>();
        try {
            monitoringCommandTemplateEntities = monitoringCommandTemplateRepository.findByCommandAndMasterAndActive(monitoringCommandEntity, isMaster, isActive);
            log.info("MonitoringCommandTemplateEntity was found\nSize - {}{}",monitoringCommandTemplateEntities.size(),DATA_SEPARATOR);
        }catch (Exception e){
            log.info("{}class MonitoringCommandService method getMonitoringCommandTemplateEntityByCommandAndIsMasterAndIsActive\nException - {}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return monitoringCommandTemplateEntities;
    }

    public byte[] getByteArrayByCommandJarFile(String jarName) {
        log.info("{}class MonitoringCommandService\nmethod getByteArrayByCommandJarFile()\n", DATA_SEPARATOR);
        byte[] jarToByte = null;
        final String jarLocation = pathCommands +"/"+ jarName;
        try {
//            JarFile command = new JarFile(jarLocation);
//            final InputStream inputStream = getClass().getResourceAsStream(jarLocation);
            log.info("{}getByteArrayByCommandJarFile path - {}{}",DATA_SEPARATOR,pathCommands,DATA_SEPARATOR);
            final InputStream inputStream = new FileInputStream(jarLocation);
            log.info(new File(pathCommands).getAbsolutePath());
            jarToByte = new byte[inputStream.available()];
            inputStream.read(jarToByte);
            inputStream.close();
        } catch (Exception e) {
            log.info("class MonitoringCommandService\nmethod getByteArrayByCommandJarFile()\nException InputStream :\n{}{}", e, DATA_SEPARATOR);
        }
        return jarToByte;
    }

    private UUID getUUIDByCommandJarFile(String jarName) {
        log.info("{}class MonitoringCommandService\nmethod getUUIDByCommandJarFile()\n", DATA_SEPARATOR);
        UUID jarFileUUID = null;
        try {
            jarFileUUID = UUID.nameUUIDFromBytes(getByteArrayByCommandJarFile(jarName));
            log.info("\nUUID by Jar File:UUID - {}\nJarName - {}{}", jarFileUUID.toString(), jarName, DATA_SEPARATOR);
        } catch (Exception e) {
            log.info("class MonitoringCommandService\nmethod getUUIDByCommandJarFile()\nException InputStream :\n{}{}", e, DATA_SEPARATOR);
        }
        return jarFileUUID;
    }


    @Scheduled(fixedRate = EVERY_MINUTE)
    private void validateMonitoringCommandEntityToDB() {
        log.info("{}class MonitoringCommandService\nmethod validateMonitoringCommandEntityToDB()\n", DATA_SEPARATOR);
        List<MonitoringCommandEntity> monitoringCommandEntities = monitoringCommandRepository.findAll();
        if (!monitoringCommandEntities.isEmpty()) {
            for (MonitoringCommandEntity monitoringCommandEntity : monitoringCommandEntities) {
                final UUID uuidFromDB = UUID.fromString(monitoringCommandEntity.getUuid());
                final UUID uuidJarFile = getUUIDByCommandJarFile(monitoringCommandEntity.getJarName());
                if (!uuidFromDB.equals(uuidJarFile)) {
                    log.info("Command Jar file changed\n");
                    try {
                        monitoringCommandEntity.setUuid(uuidJarFile.toString());
                        monitoringCommandRepository.save(monitoringCommandEntity);
                        log.info("MonitoringCommandEntity updated\n");
                    } catch (Exception e) {
                        log.info("class MonitoringCommandService\nmethod validateMonitoringCommandEntityToDB()\nException saving monitoringCommandEntity:\n{}{}", e, DATA_SEPARATOR);
                    }
                    /**
                     *  CommandEntity changed, updating all job who using this command
                     */
                    monitoringJobService.updateTimeStampToMonitoringJobEntities(monitoringJobService.getMonitoringJobEntitiesByCommand(monitoringCommandEntity));
                    log.info("Updated MonitoringJobEntities who used to last jar version{}", DATA_SEPARATOR);
                } else {
                    log.info("\nCommand Jar file is up to date{}", DATA_SEPARATOR);
                }
            }
        }else {
            log.info("\nCommands isn't exist to DB{}", DATA_SEPARATOR);
        }
    }

    public MonitoringCommandEntity getMonitoringCommandEntityById(Long id) {
        log.info("{}class MonitoringCommandService\nmethod getMonitoringCommandEntityById()\n", DATA_SEPARATOR);
        MonitoringCommandEntity monitoringCommandEntity = null;
        try {
            monitoringCommandEntity = monitoringCommandRepository.getById(id);
            log.info("{}MonitoringAgentEntity found:\nId - {}\nName - {}{}", monitoringCommandEntity.getId(), monitoringCommandEntity.getCommandName(), DATA_SEPARATOR);
        } catch (Exception e) {
            log.info("{}class MonitoringCommandService\n method getMonitoringCommandEntityById()\nException:\n{}{}", DATA_SEPARATOR, e, DATA_SEPARATOR);
        }
        return monitoringCommandEntity;
    }

    public MonitoringCommandEntity getMonitoringCommandByNameAndMasterAndActive(String commandName, boolean isActive, boolean isMaster) {
        log.info("{}class MonitoringCommandService\nmethod getMonitoringCommandByName()\n", DATA_SEPARATOR);
        MonitoringCommandEntity monitoringCommandEntity = null;
        try {
            monitoringCommandEntity = monitoringCommandRepository.getByCommandNameAndActiveAndMaster(commandName, isActive, isMaster);
            log.info("{}MonitoringAgentEntity found:\nId - {}\nName - {}{}", monitoringCommandEntity.getId(), monitoringCommandEntity.getCommandName(), DATA_SEPARATOR);
        } catch (NullPointerException e) {
            log.info("{}class MonitoringCommandService\n method getMonitoringCommandByName()\nException:\n{}{}", DATA_SEPARATOR, e, DATA_SEPARATOR);
        }
        return monitoringCommandEntity;
    }

    public MonitoringCommandEntity getMonitoringCommandEntityByUuidAndMasterAndActive(String uuid, boolean isMaster, boolean isActive) {
        log.info("{}class MonitoringCommandService\nmethod getMonitoringCommandEntityByUuidAndMasterAndActive()\n", DATA_SEPARATOR);
        MonitoringCommandEntity monitoringCommandEntity = null;
        try {
            monitoringCommandEntity = monitoringCommandRepository.getByUuidAndMasterAndActive(uuid, isMaster, isActive);
            log.info("MonitoringAgentEntity found:\nId - {}\nName - {}{}", monitoringCommandEntity.getId(), monitoringCommandEntity.getCommandName(), DATA_SEPARATOR);
        } catch (NullPointerException e) {
            log.info("{}class MonitoringCommandService\n method getMonitoringCommandEntityByUuidAndMasterAndActive()\nException:\n{}{}", DATA_SEPARATOR, e, DATA_SEPARATOR);
        }
        return monitoringCommandEntity;
    }

    public MonitoringCommandEntity getMonitoringCommandEntityByNameAndMasterAndActive(String name, boolean isMaster, boolean isActive) {
        log.info("{}class MonitoringCommandService\nmethod getMonitoringCommandEntityByUuidAndMasterAndActive()\n", DATA_SEPARATOR);
        MonitoringCommandEntity monitoringCommandEntity = null;
        try {
            monitoringCommandEntity = monitoringCommandRepository.getByCommandNameAndActiveAndMaster(name, isMaster, isActive);
            log.info("{}MonitoringAgentEntity found:\nId - {}\nName - {}{}", monitoringCommandEntity.getId(), monitoringCommandEntity.getCommandName(), DATA_SEPARATOR);
        } catch (NullPointerException e) {
            log.info("{}class MonitoringCommandService\n method getMonitoringCommandEntityByUuidAndMasterAndActive()\nException:\n{}{}", DATA_SEPARATOR, e, DATA_SEPARATOR);
        }
        return monitoringCommandEntity;
    }

    public MonitoringCommandEntity getMonitoringCommandEntityByUUID(String uuid) {
        log.info("{}class MonitoringCommandService\nmethod getMonitoringCommandEntityByUUID()\n", DATA_SEPARATOR);
        MonitoringCommandEntity monitoringCommandEntity = null;
        try {
            monitoringCommandEntity = monitoringCommandRepository.getByUuidAndMaster(uuid, true);
            log.info("{}MonitoringAgentEntity found:\nId - {}\nName - {}{}", monitoringCommandEntity.getId(), monitoringCommandEntity.getCommandName(), DATA_SEPARATOR);
        } catch (NullPointerException e) {
            log.info("{}class MonitoringCommandService\n method getMonitoringCommandEntityByUUID()\nException:\n{}{}", DATA_SEPARATOR, e, DATA_SEPARATOR);
        }
        return monitoringCommandEntity;
    }

    public MonitoringCommandEntity cloneMonitoringCommandEntity(MonitoringCommandEntity currentMonitoringCommandEntity) {
        log.info("{}class MonitoringCommandService\nmethod cloneMonitoringCommandEntity()\n", DATA_SEPARATOR);
        MonitoringCommandEntity monitoringCommandEntity = new MonitoringCommandEntity();
        try {
            monitoringCommandEntity.setUuid(currentMonitoringCommandEntity.getUuid());
            monitoringCommandEntity.setCommandName(currentMonitoringCommandEntity.getCommandName());
            monitoringCommandEntity.setCommandPath(currentMonitoringCommandEntity.getCommandPath());
            monitoringCommandEntity.setJarName(currentMonitoringCommandEntity.getJarName());
            monitoringCommandEntity.setJarPath(currentMonitoringCommandEntity.getJarPath());
            monitoringCommandEntity.setActive(false);
            monitoringCommandEntity.setMaster(false);
            monitoringCommandRepository.save(monitoringCommandEntity);
            historyService.addActionToHistory(MonitoringStatusEntity.class.getSimpleName(), "CLONE", currentMonitoringCommandEntity.getId(), monitoringCommandEntity.getId(), "cloneMonitoringCommandEntity");
            log.info("{}MonitoringAgentEntity cloned:\nId - {}\nName - {}{}", monitoringCommandEntity.getId(), monitoringCommandEntity.getCommandName(), DATA_SEPARATOR);
        } catch (Exception e) {
            log.info("{}class MonitoringCommandService\n method cloneMonitoringCommandEntity()\nException:\n{}{}", DATA_SEPARATOR, e, DATA_SEPARATOR);
        }
        return monitoringCommandEntity;
    }

    public MonitoringCommandEntity createMonitoringCommandEntity(String commandName, String commandPath, String jarName, String jarPath, boolean isActive) {
        log.info("{}class MonitoringCommandService\nmethod createMonitoringCommandEntity()\n", DATA_SEPARATOR);
        MonitoringCommandEntity monitoringCommandEntity = new MonitoringCommandEntity();
        try {
            monitoringCommandEntity.setUuid(getUUIDByCommandJarFile(jarName).toString());
            monitoringCommandEntity.setCommandName(commandName);
            monitoringCommandEntity.setCommandPath(commandPath);
            monitoringCommandEntity.setJarName(jarName);
            monitoringCommandEntity.setJarPath(jarPath);
            monitoringCommandEntity.setMaster(true);
            monitoringCommandEntity.setActive(isActive);
            monitoringCommandRepository.save(monitoringCommandEntity);
            historyService.addActionToHistory(MonitoringStatusEntity.class.getSimpleName(), "CREATE", monitoringCommandEntity.getId(), monitoringCommandEntity.getId(), "createMonitoringCommandEntity");
        } catch (Exception e) {
            log.info("{}class MonitoringCommandService\n method createMonitoringCommandEntity()\nException:\n{}{}", DATA_SEPARATOR, e, DATA_SEPARATOR);
        }
        return monitoringCommandEntity;
    }
}
