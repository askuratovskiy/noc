package com.dnipro.service.monitoring;

import com.dnipro.entity.monitoring.MonitoringStatusEntity;
import com.dnipro.repository.monitoring.MonitoringStatusRepository;
import com.dnipro.service.store.HistoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;

@Service
@Slf4j
public class MonitoringStatusService {

    private static final String DATA_SEPARATOR = "\n----------------------------------------\n";

    @Autowired
    private MonitoringStatusRepository monitoringStatusRepository;
    @Autowired
    private HistoryService historyService;

    
    public void cloneMonitoringStatusEntity(MonitoringStatusEntity cuurrentMonitoringStatusEntity){
        log.info("{}class MonitoringStatusService\n method cloneMonitoringStatusEntity()\n",DATA_SEPARATOR);
        MonitoringStatusEntity monitoringStatusEntity = new MonitoringStatusEntity();
        try {
            monitoringStatusEntity.setName(cuurrentMonitoringStatusEntity.getName());
            monitoringStatusEntity.setDescription(cuurrentMonitoringStatusEntity.getDescription());
            monitoringStatusRepository.save(monitoringStatusEntity);
            historyService.addActionToHistory(MonitoringStatusEntity.class.getSimpleName(),"CLONE",monitoringStatusEntity.getId(),monitoringStatusEntity.getId(),"cloneMonitoringStatusEntity");
            log.info("MonitoringStatusEntity cloned:\nId - {}\nName - {}\nDescription - {}{}",monitoringStatusEntity.getId(),monitoringStatusEntity.getName(),monitoringStatusEntity.getDescription(),DATA_SEPARATOR);
        }catch (Exception e){
            log.info("class MonitoringStatusService\n method cloneMonitoringStatusEntity()\nException:\n{}{}",e,DATA_SEPARATOR);
        }
    }

    
    public MonitoringStatusEntity createMonitoringStatusEntity(String name, String description){
        log.info("{}class MonitoringStatusService\n method createMonitoringStatusEntity()\n",DATA_SEPARATOR);
        MonitoringStatusEntity monitoringStatusEntity = null;
        try{
            monitoringStatusEntity = new MonitoringStatusEntity();
            monitoringStatusEntity.setName(name);
            monitoringStatusEntity.setDescription(description);
            monitoringStatusRepository.save(monitoringStatusEntity);
            historyService.addActionToHistory(MonitoringStatusEntity.class.getSimpleName(),"CREATE",monitoringStatusEntity.getId(),monitoringStatusEntity.getId(),"createMonitoringStatusEntity");
            log.info("MonitoringStatusEntity created and save:\n Status name - {}{}",name,DATA_SEPARATOR);
        }catch (Exception e){
            log.info("{}class MonitoringStatusService\n method createMonitoringStatusEntity()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return monitoringStatusEntity;
    }

    public MonitoringStatusEntity getMonitoringStatusEntityById(Long id){
        log.info("{}class MonitoringStatusService\n method getMonitoringStatusEntityById()\n",DATA_SEPARATOR);
        MonitoringStatusEntity monitoringStatusEntity = null;
        try {
            monitoringStatusEntity = monitoringStatusRepository.getOne(id);
            log.info("MonitoringStatusEntity found:\nId - {}\nName - {}\nDescription - {}{}",monitoringStatusEntity.getId(),monitoringStatusEntity.getName(),monitoringStatusEntity.getDescription(),DATA_SEPARATOR);
        }
        catch (NullPointerException e){
            log.info("MonitoringStatusEntity id - {} wasn't find{}",id,DATA_SEPARATOR);
        }catch (Exception e){
            log.info("{}class MonitoringStatusService\n method getMonitoringStatusEntityById()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return monitoringStatusEntity;
    }

    
    public MonitoringStatusEntity getMonitoringStatusEntityByName(String name){
        log.info("{}class MonitoringStatusService\n method getMonitoringStatusEntityByName()\n",DATA_SEPARATOR);
        MonitoringStatusEntity monitoringStatusEntity = null;
        try {
            monitoringStatusEntity = monitoringStatusRepository.getByName(name);
            log.info("MonitoringStatusEntity found:\nId - {}\nName - {}\nDescription - {}{}",monitoringStatusEntity.getId(),monitoringStatusEntity.getName(),monitoringStatusEntity.getDescription(),DATA_SEPARATOR);
        }
        catch (NullPointerException e){
            log.info("MonitoringStatusEntity name - {} wasn't find{}",name,DATA_SEPARATOR);
        }catch (Exception e){
            log.info("{}class MonitoringStatusService\n method getMonitoringStatusEntityByName()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        if(Objects.isNull(monitoringStatusEntity)){
            monitoringStatusEntity = createMonitoringStatusEntity(name, name);
        }
        return monitoringStatusEntity;
    }

    public MonitoringStatusEntity updateMonitoringStatusEntity(MonitoringStatusEntity currentMonitoringStatusEntity, String newName, String newDescription){
        log.info("{}class MonitoringStatusService\n method updateMonitoringStatusEntity()\n",DATA_SEPARATOR);
        cloneMonitoringStatusEntity(currentMonitoringStatusEntity);
        MonitoringStatusEntity monitoringStatusEntity = null;
        if(Objects.nonNull(currentMonitoringStatusEntity)){
            try{
                log.info("currentMonitoringStatusEntity:\nId - {}\nName - {}\nDescription - {}\n",monitoringStatusEntity.getId(),monitoringStatusEntity.getName(),monitoringStatusEntity.getDescription());
                monitoringStatusEntity = currentMonitoringStatusEntity;
                monitoringStatusEntity.setName(newName);
                monitoringStatusEntity.setDescription(newDescription);
                monitoringStatusRepository.save(monitoringStatusEntity);
                historyService.addActionToHistory(MonitoringStatusEntity.class.getSimpleName(),"UPDATE",currentMonitoringStatusEntity.getId(),monitoringStatusEntity.getId(),"updateMonitoringStatusEntity");
                log.info("MonitoringStatusEntity updated:\nId - {}\nName - {}\nDescription - {}{}",monitoringStatusEntity.getId(),monitoringStatusEntity.getName(),monitoringStatusEntity.getDescription(),DATA_SEPARATOR);
            }catch (Exception e){
                log.info("class MonitoringStatusService\n method updateMonitoringStatusEntity()\nException:\n{}{}",e,DATA_SEPARATOR);
            }
        }else {
            log.info("currentMonitoringStatusEntity is null{}",DATA_SEPARATOR);
        }
        return monitoringStatusEntity;
    }
}
