package com.dnipro.service.store;

import com.dnipro.entity.monitoring.MonitoringAgentEntity;
import com.dnipro.entity.store.DeviceEntity;
import com.dnipro.entity.store.DevicePropertiesEntity;
import com.dnipro.repository.store.DeviceEntityRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@Slf4j
public class DeviceService {

    private static final String DATA_SEPARATOR = "\n----------------------------------------\n";

    @Autowired
    private DeviceEntityRepository deviceEntityRepository;
    @Autowired
    private HistoryService historyService;

    public DeviceEntity createDeviceEntity() {
        log.info("{}class DeviceService\nmethod createDeviceEntity()\n",DATA_SEPARATOR);
        final DeviceEntity deviceEntity = new DeviceEntity();
        try {
            deviceEntity.setUuid(UUID.randomUUID().toString());
            deviceEntity.setMaster(true);
            deviceEntityRepository.save(deviceEntity);
            historyService.addActionToHistory(DeviceEntity.class.getSimpleName(),"CREATE",deviceEntity.getId(), deviceEntity.getId(),"createDeviceEntity");
            log.info("MonitoringAgentEntity created:\nId - {}\nUUID - {}{}",deviceEntity.getId(),deviceEntity.getUuid(),DATA_SEPARATOR);
        }catch (Exception e){
            log.info("{}class DeviceService\n method createDeviceEntity()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return deviceEntity;
    }

    public DeviceEntity cloneDeviceEntity(DeviceEntity currentDeviceEntity) {
        log.info("{}class DeviceService\nmethod cloneDeviceEntity()\n",DATA_SEPARATOR);
        final DeviceEntity cloneDeviceEntity = new DeviceEntity();
        try {
            cloneDeviceEntity.setUuid(currentDeviceEntity.getUuid());
            cloneDeviceEntity.setActive(currentDeviceEntity.isActive());
            cloneDeviceEntity.setMaster(false);
            cloneDeviceEntity.setProperties(currentDeviceEntity.getProperties());
            cloneDeviceEntity.setStatus(currentDeviceEntity.getStatus());
            deviceEntityRepository.save(cloneDeviceEntity);
            log.info("{}MonitoringAgentEntity cloned:\nId - {}\nUUID - {}{}",cloneDeviceEntity.getId(),cloneDeviceEntity.getUuid(),DATA_SEPARATOR);
        }catch (Exception e){
            log.info("{}class DeviceService\n method cloneDeviceEntity()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return cloneDeviceEntity;
    }

    public void updateDeviceEntity(DeviceEntity deviceEntity){
        log.info("{}class DeviceService\nmethod updateDeviceEntity()\n",DATA_SEPARATOR);
        try {
            final DeviceEntity clonedDeviceEntity = cloneDeviceEntity(deviceEntity);
            deviceEntityRepository.save(deviceEntity);
            historyService.addActionToHistory(MonitoringAgentEntity.class.getSimpleName(),"UPDATE",deviceEntity.getId(), clonedDeviceEntity.getId(),"updateDeviceEntity");
        }catch (Exception e){
            log.info("{}class DeviceService\n method updateDeviceEntity()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
    }

    public DeviceEntity getDeviceEntityById(long id){
        log.info("{}class DeviceService\nmethod getDeviceEntityById()\n",DATA_SEPARATOR);
        DeviceEntity deviceEntity = null;
        try {
            deviceEntity = deviceEntityRepository.findById(id);
            log.info("{}DeviceEntity found:\nId - {}\nUUID - {}{}",deviceEntity.getId(),deviceEntity.getUuid(),DATA_SEPARATOR);
        }catch (Exception e){
            log.info("{}class DeviceService\n method getDeviceEntityById()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return deviceEntity;
    }
    public List<DeviceEntity> getDeviceEntityByUuidAndActiveAndMaster(String uuid, boolean isActive, boolean isMaster){
        log.info("{}class DeviceService\nmethod getDeviceEntityById()\n",DATA_SEPARATOR);
        List<DeviceEntity> deviceEntity = new ArrayList<>();
        try {
            deviceEntity = deviceEntityRepository.findByUuidAndActiveAndMaster(uuid, isActive, isMaster);
        }catch (Exception e){
            log.info("{}class DeviceService\n method getDeviceEntityById()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return deviceEntity;
    }

    public List<DeviceEntity> getDeviceEntityByPropertyAndMaster(DevicePropertiesEntity devicePropertiesEntity, boolean isMaster){
        log.info("{}class DeviceService\nmethod getDeviceEntityByPropertyAndMaster()\n",DATA_SEPARATOR);
        List<DeviceEntity> deviceEntity = null;
        try {
            deviceEntity = deviceEntityRepository.findByPropertiesAndMaster(devicePropertiesEntity, true);
        }catch (Exception e){
            log.info("{}class DeviceService\n method getDeviceEntityByPropertyAndMaster()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return deviceEntity;
    }

}
