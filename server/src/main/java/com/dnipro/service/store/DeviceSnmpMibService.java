package com.dnipro.service.store;
;
import com.dnipro.entity.store.DeviceModelEntity;
import com.dnipro.entity.store.DeviceSnmpMibEntity;
import com.dnipro.entity.store.DeviceStatusEntity;
import com.dnipro.repository.store.DeviceSnmpMibEntityRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class DeviceSnmpMibService {

    private static final String DATA_SEPARATOR = "\n----------------------------------------\n";

    @Autowired
    private HistoryService historyService;
    @Autowired
    private DeviceSnmpMibEntityRepository deviceSnmpMibEntityRepository;

    private DeviceSnmpMibEntity createDeviceSnmpMibEntity() {
        log.info("{}class DeviceSnmpMibService\nmethod createDeviceSnmpMibEntity()\n", DATA_SEPARATOR);
        final DeviceSnmpMibEntity deviceSnmpMibEntity = new DeviceSnmpMibEntity();
        try {
            deviceSnmpMibEntity.setMaster(true);
            deviceSnmpMibEntityRepository.save(deviceSnmpMibEntity);
            historyService.addActionToHistory(DeviceSnmpMibEntity.class.getSimpleName(), "CREATE", deviceSnmpMibEntity.getId(), deviceSnmpMibEntity.getId(), "createDeviceSnmpMibEntity");
            log.info("MonitoringAgentEntity created:\nId - {}{}", deviceSnmpMibEntity.getId(), DATA_SEPARATOR);
        } catch (Exception e) {
            log.info("{}class DeviceSnmpMibService\n method createDeviceSnmpMibEntity()\nException:\n{}{}", DATA_SEPARATOR, e, DATA_SEPARATOR);
        }
        return deviceSnmpMibEntity;
    }

    private DeviceSnmpMibEntity createDeviceSnmpMibEntity(String name, String mib, String description, DeviceModelEntity deviceModelEntity, DeviceStatusEntity deviceStatusEntity) {
        log.info("{}class DeviceSnmpMibService\nmethod createDeviceSnmpMibEntity()\n", DATA_SEPARATOR);
        final DeviceSnmpMibEntity deviceSnmpMibEntity = new DeviceSnmpMibEntity();
        try {
            deviceSnmpMibEntity.setSnmpName(name);
            deviceSnmpMibEntity.setSnmpMib(mib);
            deviceSnmpMibEntity.setDescription(description);
            deviceSnmpMibEntity.setStatus(deviceStatusEntity);
            deviceSnmpMibEntity.setModel(deviceModelEntity);
            deviceSnmpMibEntity.setMaster(true);
            deviceSnmpMibEntityRepository.save(deviceSnmpMibEntity);
            historyService.addActionToHistory(DeviceSnmpMibEntity.class.getSimpleName(), "CREATE", deviceSnmpMibEntity.getId(), deviceSnmpMibEntity.getId(), "createDeviceSnmpMibEntity");
            log.info("MonitoringAgentEntity created:\nId - {}{}", deviceSnmpMibEntity.getId(), DATA_SEPARATOR);
        } catch (Exception e) {
            log.info("{}class DeviceSnmpMibService\n method createDeviceSnmpMibEntity()\nException:\n{}{}", DATA_SEPARATOR, e, DATA_SEPARATOR);
        }
        return deviceSnmpMibEntity;
    }

    private DeviceSnmpMibEntity cloneDeviceSnmpMibEntity(DeviceSnmpMibEntity currentDeviceSnmpMibEntity) {
        log.info("{}class DeviceSnmpMibService\nmethod cloneDeviceSnmpMibEntity()\n", DATA_SEPARATOR);
        final DeviceSnmpMibEntity cloneDeviceSnmpMibEntity = new DeviceSnmpMibEntity();
        try {
            cloneDeviceSnmpMibEntity.setStatus(currentDeviceSnmpMibEntity.getStatus());
            cloneDeviceSnmpMibEntity.setDescription(currentDeviceSnmpMibEntity.getDescription());
            cloneDeviceSnmpMibEntity.setSnmpMib(currentDeviceSnmpMibEntity.getSnmpMib());
            cloneDeviceSnmpMibEntity.setSnmpName(currentDeviceSnmpMibEntity.getSnmpName());
            cloneDeviceSnmpMibEntity.setSnmpMib(currentDeviceSnmpMibEntity.getSnmpMib());
            cloneDeviceSnmpMibEntity.setModel(currentDeviceSnmpMibEntity.getModel());
            cloneDeviceSnmpMibEntity.setMaster(false);
            deviceSnmpMibEntityRepository.save(cloneDeviceSnmpMibEntity);
            log.info("DeviceSnmpMibEntity cloned:\nId - {}{}", cloneDeviceSnmpMibEntity.getId(), DATA_SEPARATOR);
        } catch (Exception e) {
            log.info("{}class DeviceSnmpMibService\n method cloneDeviceSnmpMibEntity()\nException:\n{}{}", DATA_SEPARATOR, e, DATA_SEPARATOR);
        }
        return cloneDeviceSnmpMibEntity;
    }

    public void updateDeviceSnmpMibEntity(DeviceSnmpMibEntity deviceSnmpMibEntity) {
        log.info("{}class DeviceSnmpMibService\nmethod updateDeviceSnmpMibEntity()\n", DATA_SEPARATOR);
        try {
            final DeviceSnmpMibEntity cloneDeviceSnmpMibEntity = cloneDeviceSnmpMibEntity(deviceSnmpMibEntity);
            deviceSnmpMibEntityRepository.save(deviceSnmpMibEntity);
            historyService.addActionToHistory(DeviceSnmpMibEntity.class.getSimpleName(), "UPDATED", deviceSnmpMibEntity.getId(), cloneDeviceSnmpMibEntity.getId(), "updateDeviceSnmpMibEntity");
            log.info("DeviceSnmpMibEntity updated:\nId - {}{}", deviceSnmpMibEntity.getId(), DATA_SEPARATOR);
        } catch (Exception e) {
            log.info("{}class DeviceSnmpMibService\n method updateDeviceSnmpMibEntity()\nException:\n{}{}", DATA_SEPARATOR, e, DATA_SEPARATOR);
        }
    }

    public DeviceSnmpMibEntity getDeviceSnmpMibEntityById(long id) {
        log.info("{}class DeviceSnmpMibService\nmethod getDeviceSnmpMibEntityById()\n", DATA_SEPARATOR);
        DeviceSnmpMibEntity deviceSnmpMibEntity = null;
        try {
            deviceSnmpMibEntity = deviceSnmpMibEntityRepository.findById(id);
        } catch (Exception e) {
            log.info("{}class DeviceSnmpMibService\n method getDeviceSnmpMibEntityById()\nException:\n{}{}", DATA_SEPARATOR, e, DATA_SEPARATOR);
        }
        return deviceSnmpMibEntity;
    }

    public List<DeviceSnmpMibEntity> getDeviceSnmpMibEntityBySnmpMibAndMaster(String mib, boolean isMaster) {
        log.info("{}class DeviceSnmpMibService\nmethod getDeviceSnmpMibEntityBySnmpMibAndMaster()\n", DATA_SEPARATOR);
        List<DeviceSnmpMibEntity> deviceSnmpMibEntity = new ArrayList<>();
        try {
            deviceSnmpMibEntity = deviceSnmpMibEntityRepository.findBySnmpMibAndMaster(mib, isMaster);
        } catch (Exception e) {
            log.info("{}class DeviceSnmpMibService\n method getDeviceSnmpMibEntityBySnmpMibAndMaster()\nException:\n{}{}", DATA_SEPARATOR, e, DATA_SEPARATOR);
        }
        return deviceSnmpMibEntity;
    }

    public List<DeviceSnmpMibEntity> getDeviceSnmpMibEntityBySnmpNameAndMaster(String name, boolean isMaster) {
        log.info("{}class DeviceSnmpMibService\nmethod getDeviceSnmpMibEntityBySnmpNameAndMaster()\n", DATA_SEPARATOR);
        List<DeviceSnmpMibEntity> deviceSnmpMibEntity = new ArrayList<>();
        try {
            deviceSnmpMibEntity = deviceSnmpMibEntityRepository.findBySnmpNameAndMaster(name, isMaster);
        } catch (Exception e) {
            log.info("{}class DeviceSnmpMibService\n method getDeviceSnmpMibEntityBySnmpNameAndMaster()\nException:\n{}{}", DATA_SEPARATOR, e, DATA_SEPARATOR);
        }
        return deviceSnmpMibEntity;
    }

    public List<DeviceSnmpMibEntity> getDeviceSnmpMibEntityByDescriptionAndMaster(String description, boolean isMaster) {
        log.info("{}class DeviceSnmpMibService\nmethod getDeviceSnmpMibEntityByDescriptionAndMaster()\n", DATA_SEPARATOR);
        List<DeviceSnmpMibEntity> deviceSnmpMibEntity = new ArrayList<>();
        try {
            deviceSnmpMibEntity = deviceSnmpMibEntityRepository.findByDescriptionAndMaster(description, isMaster);
        } catch (Exception e) {
            log.info("{}class DeviceSnmpMibService\n method getDeviceSnmpMibEntityByDescriptionAndMaster()\nException:\n{}{}", DATA_SEPARATOR, e, DATA_SEPARATOR);
        }
        return deviceSnmpMibEntity;
    }

    public List<DeviceSnmpMibEntity> getDeviceSnmpMibEntityByModelAndMaster(DeviceModelEntity deviceModelEntity, boolean isMaster) {
        log.info("{}class DeviceSnmpMibService\nmethod getDeviceSnmpMibEntityByModelAndMaster()\n", DATA_SEPARATOR);
        List<DeviceSnmpMibEntity> deviceSnmpMibEntity = new ArrayList<>();
        try {
            deviceSnmpMibEntity = deviceSnmpMibEntityRepository.findByModelAndMaster(deviceModelEntity, isMaster);
        } catch (Exception e) {
            log.info("{}class DeviceSnmpMibService\n method getDeviceSnmpMibEntityByModelAndMaster()\nException:\n{}{}", DATA_SEPARATOR, e, DATA_SEPARATOR);
        }
        return deviceSnmpMibEntity;
    }

    public List<DeviceSnmpMibEntity> getDeviceSnmpMibEntityByStatusAndMaster(DeviceStatusEntity deviceStatusEntity, boolean isMaster) {
        log.info("{}class DeviceSnmpMibService\nmethod getDeviceSnmpMibEntityByStatusAndMaster()\n", DATA_SEPARATOR);
        List<DeviceSnmpMibEntity> deviceSnmpMibEntity = new ArrayList<>();
        try {
            deviceSnmpMibEntity = deviceSnmpMibEntityRepository.findByStatusAndMaster(deviceStatusEntity, isMaster);
        } catch (Exception e) {
            log.info("{}class DeviceSnmpMibService\n method getDeviceSnmpMibEntityByStatusAndMaster()\nException:\n{}{}", DATA_SEPARATOR, e, DATA_SEPARATOR);
        }
        return deviceSnmpMibEntity;
    }
}