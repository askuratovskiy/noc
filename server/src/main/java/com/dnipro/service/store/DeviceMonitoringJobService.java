package com.dnipro.service.store;

import com.dnipro.entity.monitoring.MonitoringAgentEntity;
import com.dnipro.entity.monitoring.MonitoringJobEntity;
import com.dnipro.entity.store.DeviceEntity;
import com.dnipro.entity.store.DeviceMonitoringJobEntity;
import com.dnipro.repository.store.DeviceMonitoringJobRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class DeviceMonitoringJobService {

    private static final String DATA_SEPARATOR = "\n----------------------------------------\n";

    @Autowired
    private DeviceMonitoringJobRepository deviceMonitoringJobRepository;
    @Autowired
    private HistoryService historyService;

    public DeviceMonitoringJobEntity createDeviceMonitoringJobEntity (){
        log.info("{}class DeviceMonitoringJobService\nmethod createDeviceMonitoringJobEntity()\n",DATA_SEPARATOR);
        final DeviceMonitoringJobEntity deviceMonitoringJobEntity = new DeviceMonitoringJobEntity();
        try {
            deviceMonitoringJobEntity.setMaster(true);
            deviceMonitoringJobEntity.setActive(true);
            deviceMonitoringJobRepository.save(deviceMonitoringJobEntity);
            historyService.addActionToHistory(DeviceMonitoringJobEntity.class.getSimpleName(),"CREATE",deviceMonitoringJobEntity.getId(), deviceMonitoringJobEntity.getId(),"createDeviceMonitoringJobEntity");
            log.info("DeviceMonitoringJobEntity created:\nId - {}{}",deviceMonitoringJobEntity.getId(),DATA_SEPARATOR);
        }catch (Exception e){
            log.info("{}class DeviceMonitoringJobService\n method createDeviceMonitoringJobEntity()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return  deviceMonitoringJobEntity;
    }
    public DeviceMonitoringJobEntity createDeviceMonitoringJobEntity (MonitoringJobEntity monitoringJobEntity, DeviceEntity deviceEntity){
        log.info("{}class DeviceMonitoringJobService\nmethod createDeviceMonitoringJobEntity()\n",DATA_SEPARATOR);
        final DeviceMonitoringJobEntity deviceMonitoringJobEntity = new DeviceMonitoringJobEntity();
        try {
            deviceMonitoringJobEntity.setDeviceId(deviceEntity);
            deviceMonitoringJobEntity.setMonitoringJobEntity(monitoringJobEntity);
            deviceMonitoringJobEntity.setMaster(true);
            deviceMonitoringJobEntity.setActive(true);
            deviceMonitoringJobRepository.save(deviceMonitoringJobEntity);
            historyService.addActionToHistory(MonitoringAgentEntity.class.getSimpleName(),"CREATE",deviceMonitoringJobEntity.getId(), deviceMonitoringJobEntity.getId(),"createDeviceMonitoringJobEntity");
            log.info("DeviceMonitoringJobEntity created:\nId - {}{}",deviceMonitoringJobEntity.getId(),DATA_SEPARATOR);
        }catch (Exception e){
            log.info("{}class DeviceMonitoringJobService\n method createDeviceMonitoringJobEntity()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return  deviceMonitoringJobEntity;
    }

    public DeviceMonitoringJobEntity cloneDeviceMonitoringJobEntity(DeviceMonitoringJobEntity currentDeviceMonitoringJobEntity){
        log.info("{}class DeviceMonitoringJobService\nmethod updateDeviceMonitoringJobEntity()\n",DATA_SEPARATOR);
        final DeviceMonitoringJobEntity cloneDeviceMonitoringJobEntity = new DeviceMonitoringJobEntity();
        try {
            cloneDeviceMonitoringJobEntity.setMonitoringJobEntity(currentDeviceMonitoringJobEntity.getMonitoringJobEntity());
            cloneDeviceMonitoringJobEntity.setDeviceId(currentDeviceMonitoringJobEntity.getDeviceId());
            cloneDeviceMonitoringJobEntity.setActive(currentDeviceMonitoringJobEntity.isActive());
            cloneDeviceMonitoringJobEntity.setMaster(false);
            deviceMonitoringJobRepository.save(cloneDeviceMonitoringJobEntity);
        }catch (Exception e){
            log.info("{}class DeviceMonitoringJobService\nmethod cloneDeviceMonitoringJobEntity()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }

        return cloneDeviceMonitoringJobEntity;
    }

    public void updateDeviceMonitoringJobEntity(DeviceMonitoringJobEntity deviceMonitoringJobEntity){
        log.info("{}class DeviceMonitoringJobService\nmethod updateDeviceMonitoringJobEntity()\n",DATA_SEPARATOR);
        try {
            final DeviceMonitoringJobEntity cloneDeviceMonitoringJobEntity = cloneDeviceMonitoringJobEntity(deviceMonitoringJobEntity);
            deviceMonitoringJobRepository.save(deviceMonitoringJobEntity);
            historyService.addActionToHistory(MonitoringAgentEntity.class.getSimpleName(),"UPDTATE",deviceMonitoringJobEntity.getId(), cloneDeviceMonitoringJobEntity.getId(),"updateDeviceMonitoringJobEntity");
        }catch (Exception e){
            log.info("{}class DeviceMonitoringJobService\n method updateDeviceMonitoringJobEntity()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
    }

}
