package com.dnipro.service.store;

import com.dnipro.model.server.HistoryDTO;
import com.dnipro.entity.store.HistoryActionEntity;
import com.dnipro.entity.store.HistoryEntity;
import com.dnipro.repository.store.HistoryActionEntityRepository;
import com.dnipro.repository.store.HistoryEntityRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Slf4j
@Service
public class HistoryService {

    private static final String DATA_SEPARATOR = "\n----------------------------------------\n";

    @Autowired
    HistoryEntityRepository historyEntityRepository;
    @Autowired
    HistoryActionEntityRepository historyActionEntityRepository;

    public void addActionToHistory(String entityName, String actionStatus, Long currentId, Long newId, String note) {
        log.info("{}class HistoryService\n method addActionToHistory()\n",DATA_SEPARATOR);
        final HistoryDTO historyDTO = HistoryDTO.builder()
                .entityName(entityName)
                .action(actionStatus)
                .currentId(currentId)
                .newId(newId)
                .note(note)
                .build();
        createHistoryEntity(historyDTO);
        log.info("Calling createHistoryEntity{}",DATA_SEPARATOR);
    }

    
    public void createHistoryEntity(HistoryDTO historyDTO) {
        log.info("{}class HistoryService\n method createHistoryEntity()\n",DATA_SEPARATOR);
        final HistoryEntity historyEntity = new HistoryEntity();
        try {
            historyEntity.setEntityNewId(historyDTO.getNewId());
            historyEntity.setEntityOldId(historyDTO.getCurrentId());
            historyEntity.setEntityName(historyDTO.getEntityName());
            historyEntity.setNote(historyDTO.getNote());
            HistoryActionEntity historyActionEntity = getHistoryActionEntityByName(historyDTO.getAction());
            if(Objects.isNull(historyActionEntity)){
                historyActionEntity = createHistoryActionEntity(historyDTO.getAction());
            }
            historyEntity.setAction(historyActionEntity);
            historyEntityRepository.save(historyEntity);
            log.info("HistoryActionEntity created\nId - {}\nActionName - {}{}",historyEntity.getId(),historyEntity.getAction().getActionName(),DATA_SEPARATOR);
        }catch (Exception e){
            log.info("{}class HistoryService\n method createHistoryEntity()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        log.info("HistoryActionEntity created\nId - {}\nActionName - {}{}",historyEntity.getId(),historyEntity.getAction().getActionName(),DATA_SEPARATOR);
    }

    public HistoryEntity getHistoryEntityById(Long id){
        log.info("{}class HistoryService\n method getHistoryEntityById()\n",DATA_SEPARATOR);
        HistoryEntity historyEntity = null;
        try{
            historyEntity = historyEntityRepository.getOne(id);
            log.info("HistoryActionEntity got\nId - {}\nActionName - {}{}",historyEntity.getId(),historyEntity.getAction().getActionName(),DATA_SEPARATOR);
        }catch (Exception e){
            log.info("{}class HistoryService\n method getHistoryEntityById()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return historyEntity;
    }

    public List<HistoryEntity> getHistoryEntityByEntityOldId(Long id){
        log.info("{}class HistoryService\n method getHistoryEntityByEntityOldId(){}\n",DATA_SEPARATOR,DATA_SEPARATOR);
        List<HistoryEntity> historyEntity = new ArrayList<>();
        try{
            historyEntity = historyEntityRepository.findByEntityOldId(id);
        }catch (Exception e){
            log.info("{}class HistoryService\n method getHistoryEntityByEntityOldId()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return historyEntity;
    }

    public List<HistoryEntity> getHistoryEntityByEntityNewId(Long id){
        log.info("{}class HistoryService\n method getHistoryEntityByEntityNewId(){}\n",DATA_SEPARATOR,DATA_SEPARATOR);
        List<HistoryEntity> historyEntity = new ArrayList<>();
        try{
            historyEntity = historyEntityRepository.findByEntityNewId(id);
        }catch (Exception e){
            log.info("{}class HistoryService\n method getHistoryEntityByEntityNewId()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return historyEntity;
    }

    
    public HistoryActionEntity createHistoryActionEntity(String name){
        log.info("{}class HistoryService\n method createHistoryActionEntity()\n",DATA_SEPARATOR);
        HistoryActionEntity historyActionEntity = new HistoryActionEntity();
        try {
            historyActionEntity.setActionName(name);
            historyActionEntityRepository.save(historyActionEntity);
            addActionToHistory(HistoryActionEntity.class.getSimpleName(), "CREATE", historyActionEntity.getId(),historyActionEntity.getId(),"createHistoryActionEntity");
            log.info("HistoryActionEntity created\nId - {}\nName - {}\nDescription - {}{}",historyActionEntity.getId(),historyActionEntity.getActionName(),historyActionEntity.getDescription(),DATA_SEPARATOR);
        }catch (Exception e){
            log.info("{}class HistoryService\n method createHistoryActionEntity()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return historyActionEntity;
    }

    
    public HistoryActionEntity getHistoryActionEntityByName(String name) {
        log.info("{}class HistoryService\n method getHistoryActionEntityByName()\n",DATA_SEPARATOR);
        HistoryActionEntity historyActionEntity = null;
        try {
            historyActionEntity = historyActionEntityRepository.findByActionName(name);
            log.info("HistoryActionEntity got\nId - {}\nName - {}\nDescription - {}{}",historyActionEntity.getId(),historyActionEntity.getActionName(),historyActionEntity.getDescription(),DATA_SEPARATOR);
        }catch (Exception e){
            log.info("{}class HistoryService\n method getHistoryActionEntityByName()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return historyActionEntity;
    }

    private HistoryActionEntity getHistoryActionEntityById(Long id) {
        log.info("{}class HistoryService\n method getHistoryActionEntityById()\n",DATA_SEPARATOR);
        HistoryActionEntity historyActionEntity = null;
        try {
            historyActionEntity = historyActionEntityRepository.getOne(id);
            log.info("HistoryActionEntity got\nId - {}\nName - {}\nDescription - {}{}",historyActionEntity.getId(),historyActionEntity.getActionName(),historyActionEntity.getDescription(),DATA_SEPARATOR);
        }catch (Exception e){
            log.info("{}class HistoryService\n method getHistoryActionEntityById()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return historyActionEntity;
    }
}
