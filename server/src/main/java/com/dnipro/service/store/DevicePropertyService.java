package com.dnipro.service.store;

import com.dnipro.entity.monitoring.MonitoringAgentEntity;
import com.dnipro.entity.store.DevicePropertiesEntity;
import com.dnipro.entity.store.DeviceStatusEntity;
import com.dnipro.repository.store.DevicePropertiesEntityRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Service
@Slf4j
public class DevicePropertyService {

    private static final String DATA_SEPARATOR = "\n----------------------------------------\n";
    
    @Autowired
    private DevicePropertiesEntityRepository devicePropertiesEntityRepository;
    @Autowired
    private HistoryService historyService;

    private final Lock lockTrans = new ReentrantLock();

    public DevicePropertiesEntity createDevicePropertiesEntity() {
        log.info("{}class DevicePropertyService\nmethod createDevicePropertiesEntity()\n",DATA_SEPARATOR);
        final DevicePropertiesEntity devicePropertiesEntity = new DevicePropertiesEntity();
        try {
            devicePropertiesEntity.setMaster(true);
            devicePropertiesEntityRepository.save(devicePropertiesEntity);
            historyService.addActionToHistory(DevicePropertiesEntity.class.getSimpleName(),"CREATE",devicePropertiesEntity.getId(), devicePropertiesEntity.getId(),"createDevicePropertiesEntity");
            log.info("DevicePropertiesEntity created:\nId - {}{}",devicePropertiesEntity.getId(),DATA_SEPARATOR);
        }catch (Exception e){
            log.info("{}class DevicePropertyService\n method createDevicePropertiesEntity()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return devicePropertiesEntity;
    }

    public DevicePropertiesEntity cloneDevicePropertiesEntity(DevicePropertiesEntity currentDevicePropertiesEntity) {
        log.info("{}class DevicePropertyService\nmethod cloneDevicePropertiesEntity()\n",DATA_SEPARATOR);
        final DevicePropertiesEntity cloneDevicePropertiesEntity = new DevicePropertiesEntity();
        try {
            cloneDevicePropertiesEntity.setHostName(currentDevicePropertiesEntity.getHostName());
            cloneDevicePropertiesEntity.setFirmwareVersion(currentDevicePropertiesEntity.getFirmwareVersion());
            cloneDevicePropertiesEntity.setHardwareVersion(currentDevicePropertiesEntity.getHardwareVersion());
            cloneDevicePropertiesEntity.setIpAddress(currentDevicePropertiesEntity.getIpAddress());
            cloneDevicePropertiesEntity.setMacAddress(currentDevicePropertiesEntity.getMacAddress());
            cloneDevicePropertiesEntity.setSerialNumber(currentDevicePropertiesEntity.getSerialNumber());
            cloneDevicePropertiesEntity.setStatus(currentDevicePropertiesEntity.getStatus());
            cloneDevicePropertiesEntity.setMaster(false);
            devicePropertiesEntityRepository.save(cloneDevicePropertiesEntity);
            log.info("{}MonitoringAgentEntity cloned:\nId - {}{}",currentDevicePropertiesEntity.getId(),DATA_SEPARATOR);
        }catch (Exception e){
            log.info("{}class DevicePropertyService\n method cloneDevicePropertiesEntity()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return cloneDevicePropertiesEntity;
    }
    
    public void  updateDevicePropertiesEntity(DevicePropertiesEntity devicePropertiesEntity){
        log.info("{}class DevicePropertyService\nmethod updateDevicePropertiesEntity()\n",DATA_SEPARATOR);
        try {
            final DevicePropertiesEntity clonedDevicePropertiesEntity = cloneDevicePropertiesEntity(devicePropertiesEntity);
            devicePropertiesEntityRepository.save(devicePropertiesEntity);
            historyService.addActionToHistory(DevicePropertiesEntity.class.getSimpleName(),"UPDATE",devicePropertiesEntity.getId(), clonedDevicePropertiesEntity.getId(),"updateDevicePropertiesEntity");
            if(devicePropertiesEntity.isMaster()){
                lockTrans.unlock();
            }
        }catch (Exception e){
            log.info("{}class DevicePropertyService\n method updateDevicePropertiesEntity()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
    }

    public void updateSerialNumberToDevicePropertiesEntity(DevicePropertiesEntity devicePropertiesEntity){
        log.info("{}class DevicePropertyService\nmethod updateSerialNumberToDevicePropertiesEntity()\n",DATA_SEPARATOR);
        try {
            final DevicePropertiesEntity clonedDevicePropertiesEntity = cloneDevicePropertiesEntity(devicePropertiesEntity);
            devicePropertiesEntityRepository.save(devicePropertiesEntity);
            historyService.addActionToHistory(DevicePropertiesEntity.class.getSimpleName(),"UPDATE",devicePropertiesEntity.getId(), clonedDevicePropertiesEntity.getId(),"updateDevicePropertiesEntity");
        }catch (Exception e){
            log.info("{}class DevicePropertyService\n method updateSerialNumberToDevicePropertiesEntity()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
    }
    
    public DevicePropertiesEntity getDevicePropertiesEntityById(long id){
        log.info("{}class DevicePropertyService\nmethod getDevicePropertiesEntityById()\n",DATA_SEPARATOR);
        DevicePropertiesEntity devicePropertiesEntity = null;
        try {
            devicePropertiesEntity = devicePropertiesEntityRepository.findById(id);
            log.info("{}DevicePropertiesEntity found:\nId - {}{}",devicePropertiesEntity.getId(),DATA_SEPARATOR);
        }catch (Exception e){
            log.info("{}class DevicePropertyService\n method getDevicePropertiesEntityById()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return devicePropertiesEntity;
    }
    public List<DevicePropertiesEntity> getDevicePropertiesEntityByIpAddressAndMaster(String ipAddress, boolean isMaster){
        log.info("{}class DevicePropertyService\nmethod getDevicePropertiesEntityByIpAddressAndMaster()\n",DATA_SEPARATOR);
        List<DevicePropertiesEntity> devicePropertiesEntity = new ArrayList<>();
        try {
            devicePropertiesEntity = devicePropertiesEntityRepository.findByIpAddressAndMaster(ipAddress, isMaster);
        }catch (Exception e){
            log.info("{}class DevicePropertyService\n method getDevicePropertiesEntityByIpAddressAndMaster()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return devicePropertiesEntity;
    }

    public List<DevicePropertiesEntity> getForUpdateDevicePropertiesEntityByIpAddressAndMaster(String ipAddress, boolean isMaster){
        log.info("{}class DevicePropertyService\nmethod getDevicePropertiesEntityByIpAddressAndMaster()\n",DATA_SEPARATOR);
        List<DevicePropertiesEntity> devicePropertiesEntity = new ArrayList<>();
        try {
            if(isMaster){
                lockTrans.lock();
            }
            devicePropertiesEntity = devicePropertiesEntityRepository.findByIpAddressAndMaster(ipAddress, isMaster);
        }catch (Exception e){
            log.info("{}class DevicePropertyService\n method getDevicePropertiesEntityByIpAddressAndMaster()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return devicePropertiesEntity;
    }

    public List<DevicePropertiesEntity> getDevicePropertiesEntityByHostNameAndMaster(String hostName, boolean isMaster){
        log.info("{}class DevicePropertyService\nmethod getDevicePropertiesEntityByHostNameAndMaster()\n",DATA_SEPARATOR);
        List<DevicePropertiesEntity> devicePropertiesEntity = new ArrayList<>();
        try {
            devicePropertiesEntity = devicePropertiesEntityRepository.findByHostNameAndMaster(hostName, isMaster);
        }catch (Exception e){
            log.info("{}class DevicePropertyService\n method getDevicePropertiesEntityByHostNameAndMaster()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return devicePropertiesEntity;
    }

    public List<DevicePropertiesEntity> getDevicePropertiesEntityByMacAddressAndMaster(String macAddress, boolean isMaster){
        log.info("{}class DevicePropertyService\nmethod getDevicePropertiesEntityByMacAddressAndMaster()\n",DATA_SEPARATOR);
        List<DevicePropertiesEntity> devicePropertiesEntity = new ArrayList<>();
        try {
            devicePropertiesEntity = devicePropertiesEntityRepository.findByMacAddressAndMaster(macAddress, isMaster);
        }catch (Exception e){
            log.info("{}class DevicePropertyService\n method getDevicePropertiesEntityByMacAddressAndMaster()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return devicePropertiesEntity;
    }

    public List<DevicePropertiesEntity> getDevicePropertiesEntityBySerialNumberAndMaster(String serialNumber, boolean isMaster){
        log.info("{}class DevicePropertyService\nmethod getDevicePropertiesEntityBySerialNumberAndMaster()\n",DATA_SEPARATOR);
        List<DevicePropertiesEntity> devicePropertiesEntity = new ArrayList<>();
        try {
            devicePropertiesEntity = devicePropertiesEntityRepository.findBySerialNumberAndMaster(serialNumber, isMaster);
        }catch (Exception e){
            log.info("{}class DevicePropertyService\n method getDevicePropertiesEntityBySerialNumberAndMaster()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return devicePropertiesEntity;
    }

    public List<DevicePropertiesEntity> getDevicePropertiesEntityByIpAddressAndStatusAndMaster(String ipAddress, DeviceStatusEntity deviceStatusEntity, boolean isMaster){
        log.info("{}class DevicePropertyService\nmethod getDevicePropertiesEntityByIpAddressAndStatusAndMaster()\n",DATA_SEPARATOR);
        List<DevicePropertiesEntity> devicePropertiesEntity = new ArrayList<>();
        try {
            devicePropertiesEntity = devicePropertiesEntityRepository.findByIpAddressAndStatusAndMaster(ipAddress, deviceStatusEntity, isMaster);
        }catch (Exception e){
            log.info("{}class DevicePropertyService\n method getDevicePropertiesEntityByIpAddressAndStatusAndMaster()\nException:\n{}{}",DATA_SEPARATOR,e,DATA_SEPARATOR);
        }
        return devicePropertiesEntity;
    }
    
}
