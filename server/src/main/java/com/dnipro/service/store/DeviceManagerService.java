package com.dnipro.service.store;

import com.dnipro.repository.store.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class DeviceManagerService {

    @Autowired
    private HistoryService historyService;
    @Autowired
    private DeviceEntityRepository deviceEntityRepository;
    @Autowired
    private DevicePropertiesEntityRepository devicePropertiesEntityRepository;
    @Autowired
    private DeviceModelEntityRepository deviceModelEntityRepository;
    @Autowired
    private DeviceTypeEntityRepository deviceTypeEntityRepository;
    @Autowired
    private DeviceVendorEntityRepository deviceVendorEntityRepository;
    @Autowired
    private DeviceSnmpMibEntityRepository deviceSnmpMibEntityRepository;
    @Autowired
    private DeviceSubnetEntityRepository deviceSubnetEntityRepository;
    @Autowired
    private DeviceStatusEntityRepository deviceStatusEntityRepository;
    @Autowired
    private DeviceMonitoringJobRepository deviceMonitoringJobRepository;
    @Autowired
    private DeviceService deviceService;


//    public DevicePropertiesEntity createDeviceProperties() {
//        DevicePropertiesEntity devicePropertiesEntity = new DevicePropertiesEntity();
//        devicePropertiesEntity.setStatus(getDeviceStatusByStatusName("CREATE"));
//        devicePropertiesEntity.setMaster(true);
//        devicePropertiesEntityRepository.save(devicePropertiesEntity);
//        historyService.addActionToHistory(
//                DevicePropertiesEntity.class.getSimpleName(),
//                "CREATE_DEVICE_PROPERTIES",
//                devicePropertiesEntity.getId(),
//                devicePropertiesEntity.getId(),
//                "CREATE_DEVICE_PROPERTIES"
//        );
//        return devicePropertiesEntity;
//    }

//    public  DeviceStatusEntity createDeviceStatus(String statusName, String description){
//        DeviceStatusEntity deviceStatusEntity = new DeviceStatusEntity();
//        deviceStatusEntity.setStatusName(statusName);
//        deviceStatusEntity.setDescription(description);
//        deviceStatusEntityRepository.save(deviceStatusEntity);
//        historyService.addActionToHistory(
//                DeviceStatusEntity.class.getSimpleName(),
//                "CREATE_DEVICE_STATUS",
//                deviceStatusEntity.getId(),
//                deviceStatusEntity.getId(),
//                "CREATE_DEVICE_STATUS"
//        );
//        return deviceStatusEntity;
//    }
//    public void updateStatusNameToDeviceStatus(DeviceStatusEntity deviceStatusEntity, String statusName) {
//        deviceStatusEntity.setStatusName(statusName);
//        deviceStatusEntityRepository.save(deviceStatusEntity);
//        historyService.addActionToHistory(
//                DeviceStatusEntity.class.getSimpleName(),
//                "UPDATE_STATUS_NAME_TO_DEVICE_STATUS",
//                deviceStatusEntity.getId(),
//                deviceStatusEntity.getId(),
//                "UPDATE_STATUS_NAME_TO_DEVICE_STATUS"
//        );
//    }

//    public DeviceMonitoringJobEntity createDeviceService(DeviceEntity deviceEntity, MonitoringJobEntity monitoringJobEntity){
//        DeviceMonitoringJobEntity deviceMonitoringJobEntity = new DeviceMonitoringJobEntity();
//        deviceMonitoringJobEntity.setDeviceId(deviceEntity);
//        deviceMonitoringJobEntity.setMonitoringJobEntity(monitoringJobEntity);
//        deviceMonitoringJobRepository.save(deviceMonitoringJobEntity);
//
//        historyService.addActionToHistory(
//                DeviceMonitoringJobEntity.class.getSimpleName(),
//                "createDeviceService",
//                deviceMonitoringJobEntity.getId(),
//                deviceMonitoringJobEntity.getId(),
//                "createDeviceService"
//        );
//        return deviceMonitoringJobEntity;
//    }

//    public DeviceStatusEntity getDeviceStatusByStatusName(String statusName) {
//        DeviceStatusEntity deviceStatusEntity;
//        if (Objects.nonNull(deviceStatusEntityRepository.findByStatusName(statusName))) {
//            deviceStatusEntity = deviceStatusEntityRepository.findByStatusName(statusName);
//        } else {
//            DeviceStatusEntity deviceStatusEntityNew = createDeviceStatus(statusName, statusName);
//            deviceStatusEntityRepository.save(deviceStatusEntityNew);
//            deviceStatusEntity = deviceStatusEntityNew;
//        }
//        return deviceStatusEntity;
//    }

//    public DeviceEntity cloneDevice(DeviceEntity deviceEntityOld) {
//        DeviceEntity cloneDeviceEntity = new DeviceEntity();
//        cloneDeviceEntity.setUuid(deviceEntityOld.getUuid());
//        cloneDeviceEntity.setStatus(deviceEntityOld.getStatus());
//        cloneDeviceEntity.setProperties(deviceEntityOld.getProperties());
//        cloneDeviceEntity.setModel(deviceEntityOld.getModel());
//        cloneDeviceEntity.setActive(deviceEntityOld.isActive());
//        cloneDeviceEntity.setStatus(getDeviceStatusByStatusName("UPDATE"));
//        cloneDeviceEntity.setMaster(false);
//        deviceEntityRepository.save(cloneDeviceEntity);
//
//        historyService.addActionToHistory(
//                DeviceEntity.class.getSimpleName(),
//                "CLONE_DEVICE_UPDATE",
//                deviceEntityOld.getId(),
//                cloneDeviceEntity.getId(),
//                "CLONE_DEVICE_UPDATE"
//        );
//        return cloneDeviceEntity;
//    }

    
//    public DevicePropertiesEntity cloneDeviceProperties(DevicePropertiesEntity devicePropertiesEntityOld) {
//        DevicePropertiesEntity devicePropertiesEntityNew = new DevicePropertiesEntity();
//        devicePropertiesEntityNew.setIpAddress(devicePropertiesEntityOld.getIpAddress());
//        devicePropertiesEntityNew.setSerialNumber(devicePropertiesEntityOld.getSerialNumber());
//        devicePropertiesEntityNew.setMacAddress(devicePropertiesEntityOld.getMacAddress());
//        devicePropertiesEntityNew.setHostName(devicePropertiesEntityOld.getHostName());
//        devicePropertiesEntityNew.setHardwareVersion(devicePropertiesEntityOld.getHardwareVersion());
//        devicePropertiesEntityNew.setFirmwareVersion(devicePropertiesEntityOld.getFirmwareVersion());
//        devicePropertiesEntityNew.setStatus(getDeviceStatusByStatusName("UPDATE"));
//        devicePropertiesEntityRepository.save(devicePropertiesEntityNew);
//        historyService.addActionToHistory(
//                DevicePropertiesEntity.class.getSimpleName(),
//                "CLONE_DEVICE_PROPERTIES_UPDATE",
//                devicePropertiesEntityOld.getId(),
//                devicePropertiesEntityNew.getId(),
//                "CLONE_DEVICE_PROPERTIES_UPDATE"
//        );
//        return devicePropertiesEntityNew;
//    }
//
//    public void addPropertiesToDevice(DeviceEntity deviceEntityOld, DevicePropertiesEntity devicePropertiesEntity) {
//        DeviceEntity deviceEntityNew = cloneDevice(deviceEntityOld);
//        deviceEntityOld.setProperties(devicePropertiesEntity);
//        deviceEntityRepository.save(deviceEntityOld);
//
//        historyService.addActionToHistory(
//                DeviceEntity.class.getSimpleName(),
//                "ADD_PROPERTIES_TO_DEVICE",
//                deviceEntityOld.getId(),
//                deviceEntityNew.getId(),
//                "ADD_PROPERTIES_TO_DEVICE"
//        );
//    }

//    @Transactional
//    public void updateIpAddressToDeviceProperties(DevicePropertiesEntity devicePropertiesEntityOld, String ipAddress) {
//        DevicePropertiesEntity devicePropertiesEntityNew = cloneDeviceProperties(devicePropertiesEntityOld);
//        devicePropertiesEntityOld.setIpAddress(ipAddress);
//        devicePropertiesEntityRepository.save(devicePropertiesEntityOld);
//        historyService.addActionToHistory(
//                DevicePropertiesEntity.class.getSimpleName(),
//                "UPDATE_IPADDRESS_TO_DEVICE_PROPERTIES",
//                devicePropertiesEntityOld.getId(),
//                devicePropertiesEntityNew.getId(),
//                "UPDATE_IPADDRESS_TO_DEVICE_PROPERTIES"
//        );
//    }

//    @Transactional
//    public void updateActiveToDevice(DeviceEntity deviceEntityOld, boolean state) {
//        DeviceEntity DeviceEntityNew = cloneDevice(deviceEntityOld);
//        deviceEntityOld.setActive(state);
//        deviceEntityRepository.save(deviceEntityOld);
//
//        historyService.addActionToHistory(
//                DeviceEntity.class.getSimpleName(),
//                "UPDATE_ACTIVE_TO_DEVICE",
//                deviceEntityOld.getId(),
//                DeviceEntityNew.getId(),
//                "UPDATE_ACTIVE_TO_DEVICE"
//        );
//    }

//    public boolean isIpAddressByStatusNameToDeviceProperties(String ipAddress, DeviceStatusEntity deviceStatusEntity) {
//        boolean isIpAddress = false;
//        if (Objects.nonNull(devicePropertiesEntityRepository.findByIpAddressAndStatus(ipAddress, deviceStatusEntity))) {
//            isIpAddress = true;
//        }
//        return isIpAddress;
//    }


//    public boolean isIpAddressToDeviceProperties(String ipAddress) {
//        boolean isIpAddress = false;
//        if (Objects.nonNull(devicePropertiesEntityRepository.findByIpAddressAndMaster(ipAddress, true))) {
//            isIpAddress = true;
//        }
//        return isIpAddress;
//    }
//
//    public boolean isActiveByStatusToDevice(boolean newActive, DeviceStatusEntity deviceStatusEntity){
//        boolean currentActive = false;
//        if(Objects.nonNull(deviceEntityRepository.findByActiveAndStatus(newActive, deviceStatusEntity))){
//            currentActive = true;
//        }
//        return currentActive;
//    }

//    @Transactional
//    public DeviceEntity getDevicesMasterByPropperties(DevicePropertiesEntity devicePropertiesEntity){
//        DeviceEntity deviceEntity = deviceEntityRepository.findByPropertiesAndMaster(devicePropertiesEntity, true);
//        return deviceEntity;
//    }
//
//    public DeviceEntity getDeviceByProppertiesByStatus(DevicePropertiesEntity devicePropertiesEntity, DeviceStatusEntity deviceStatusEntity){
//        DeviceEntity deviceEntity = deviceEntityRepository.findByPropertiesAndStatus(devicePropertiesEntity, deviceStatusEntity);
//        return deviceEntity;
//    }
//    public DeviceEntity getDeviceMasterByPropperties(DevicePropertiesEntity devicePropertiesEntity){
//        DeviceEntity deviceEntity = deviceEntityRepository.findByPropertiesAndMaster(devicePropertiesEntity, true);
//        return deviceEntity;
//    }
//
//    public DevicePropertiesEntity getDevicePropertiesByIpAddressAndStatusName(String ipAddress,  DeviceStatusEntity deviceStatusEntity){
//        DevicePropertiesEntity devicePropertiesEntity = devicePropertiesEntityRepository.findByIpAddressAndStatus(ipAddress, deviceStatusEntity);
//        return devicePropertiesEntity;
//    }
//    @Transactional
//    public DevicePropertiesEntity getDevicePropertiesByIpAddress(String ipAddress){
//        DevicePropertiesEntity devicePropertiesEntity = devicePropertiesEntityRepository.findByIpAddressAndMaster(ipAddress, true);
//        return devicePropertiesEntity;
//    }
//    public DevicePropertiesEntity getDevicePropertiesMasterByIpAddress(String ipAddress){
//        DevicePropertiesEntity devicePropertiesEntity = devicePropertiesEntityRepository.findByIpAddressAndMaster(ipAddress, true);
//        return devicePropertiesEntity;
//    }
}
