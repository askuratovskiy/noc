package com.dnipro.service.store;

import com.dnipro.repository.store.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LocationService {

    @Autowired
    LocationEntityRepository locationEntityRepository;
    @Autowired
    LocationCityEntityRepository locationCityEntityRepository;
    @Autowired
    LocationDistrictEntityRepository locationDistrictEntityRepository;
    @Autowired
    LocationStreetEntityRepository locationStreetEntityRepository;
    @Autowired
    LocalizationEntityRepository localizationEntityRepository;
}
